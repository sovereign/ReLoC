From stdpp Require Import strings gmap mapset stringmap.
From iris_logrel.F_mu_ref_conc Require Export lang subst.
Import lang.

Module R.
Inductive expr :=
  | Val (v : lang.val) (e : lang.expr) (Hev : to_val e = Some v)
  | ClosedExpr (e : lang.expr) `{Closed ∅ e}
  | Var (x : string)
  | Rec (f x : binder) (e : expr)
  | App (e1 e2 : expr)
  (* Base Types *)
  | Lit (l : literal)
  | BinOp (op : binop) (e1 e2 : expr)
  (* If then else *)
  | If (e0 e1 e2 : expr)
  (* Products *)
  | Pair (e1 e2 : expr)
  | Fst (e : expr)
  | Snd (e : expr)
  (* Sums *)
  | InjL (e : expr)
  | InjR (e : expr)
  | Case (e0 : expr) (e1 : expr) (e2 : expr)
  (* Recursive Types *)
  | Fold (e : expr)
  | Unfold (e : expr)
  (* Polymorphic Types *)
  | TLam (e : expr)
  | TApp (e : expr)
  (* Existential types *)
  | Pack (e : expr)
  | Unpack (e : expr) (e' : expr)
  (* Concurrency *)
  | Fork (e : expr)
  (* Reference Types *)
  | Alloc (e : expr)
  | Load (e : expr)
  | Store (e1 : expr) (e2 : expr)
  (* Compare and swap used for fine-grained concurrency *)
  | CAS (e0 : expr) (e1 : expr) (e2 : expr).

Fixpoint to_expr (e : expr) : lang.expr :=
  match e with
  | Val v e _ => e
  | ClosedExpr e => e
  | Var x => lang.Var x
  | Rec f x e => lang.Rec f x (to_expr e)
  | App e1 e2 => lang.App (to_expr e1) (to_expr e2)
  | Lit l => lang.Lit l
  | BinOp op e1 e2 => lang.BinOp op (to_expr e1) (to_expr e2)
  | If e0 e1 e2 => lang.If (to_expr e0) (to_expr e1) (to_expr e2)
  | Pair e1 e2 => lang.Pair (to_expr e1) (to_expr e2)
  | Fst e => lang.Fst (to_expr e)
  | Snd e => lang.Snd (to_expr e)
  | InjL e => lang.InjL (to_expr e)
  | InjR e => lang.InjR (to_expr e)
  | Case e0 e1 e2 => lang.Case (to_expr e0) (to_expr e1) (to_expr e2)
  | Fork e => lang.Fork (to_expr e)
  | Alloc e => lang.Alloc (to_expr e)
  | Load e => lang.Load (to_expr e)
  | Store e1 e2 => lang.Store (to_expr e1) (to_expr e2)
  | CAS e0 e1 e2 => lang.CAS (to_expr e0) (to_expr e1) (to_expr e2)
  | Fold e => lang.Fold (to_expr e)
  | Unfold e => lang.Unfold (to_expr e)
  | Pack e => lang.Pack (to_expr e)
  | Unpack e e' => lang.Unpack (to_expr e) (to_expr e')
  | TLam e => lang.TLam (to_expr e)
  | TApp e => lang.TApp (to_expr e)
  end.

Ltac of_expr e :=
  lazymatch e with
  | lang.Var ?x => constr:(Var x)
  | lang.Rec ?f ?x ?e => let e := of_expr e in constr:(Rec f x e)
  | lang.App ?e1 ?e2 =>
     let e1 := of_expr e1 in let e2 := of_expr e2 in constr:(App e1 e2)
  | lang.Lit ?l => constr:(Lit l)
  | lang.BinOp ?op ?e1 ?e2 =>
     let e1 := of_expr e1 in let e2 := of_expr e2 in constr:(BinOp op e1 e2)
  | lang.If ?e0 ?e1 ?e2 =>
     let e0 := of_expr e0 in let e1 := of_expr e1 in let e2 := of_expr e2 in
     constr:(If e0 e1 e2)
  | lang.Pair ?e1 ?e2 =>
     let e1 := of_expr e1 in let e2 := of_expr e2 in constr:(Pair e1 e2)
  | lang.Fst ?e => let e := of_expr e in constr:(Fst e)
  | lang.Snd ?e => let e := of_expr e in constr:(Snd e)
  | lang.InjL ?e => let e := of_expr e in constr:(InjL e)
  | lang.InjR ?e => let e := of_expr e in constr:(InjR e)
  | lang.Case ?e0 ?e1 ?e2 =>
     let e0 := of_expr e0 in let e1 := of_expr e1 in let e2 := of_expr e2 in
     constr:(Case e0 e1 e2)
  | lang.Fork ?e => let e := of_expr e in constr:(Fork e)
  | lang.Alloc ?e => let e := of_expr e in constr:(Alloc e)
  | lang.Load ?e => let e := of_expr e in constr:(Load e)
  | lang.Store ?e1 ?e2 =>
     let e1 := of_expr e1 in let e2 := of_expr e2 in constr:(Store e1 e2)
  | lang.CAS ?e0 ?e1 ?e2 =>
     let e0 := of_expr e0 in let e1 := of_expr e1 in let e2 := of_expr e2 in
     constr:(CAS e0 e1 e2)
  | lang.Fold ?e => let e := of_expr e in constr:(Fold e)
  | lang.Unfold ?e => let e := of_expr e in constr:(Unfold e)
  | lang.Pack ?e => let e := of_expr e in constr:(Pack e)
  | lang.Unpack ?e ?e' =>
     let e := of_expr e in let e' := of_expr e' in constr:(Unpack e e')
  | lang.TLam ?e => let e := of_expr e in constr:(TLam e)
  | lang.TApp ?e => let e := of_expr e in constr:(TApp e)
  | to_expr ?e => e
  | of_val ?v => constr:(Val v (of_val v) (to_of_val v))
  | _ =>
    lazymatch goal with
    | [H : to_val e = Some ?ev |- _] =>
      constr:(Val ev e H)
    | [H : Closed ∅ e |- _] => constr:(@ClosedExpr e H)
    | [H : Is_true (is_closed ∅ e) |- _] => constr:(@ClosedExpr e H)
    end
  end.

Fixpoint is_closed (X : stringset) (e : expr) : bool :=
  match e with
  | Val _ _ _ => true
  | ClosedExpr e => true
  | Var x => bool_decide (x ∈ X)
  | Lit l => true
  | Rec f x e => is_closed (x :b: f :b: X) e
  | App e1 e2 | BinOp _ e1 e2 | Pair e1 e2 | Store e1 e2 | Unpack e1 e2 =>
    is_closed X e1 && is_closed X e2
  | If e0 e1 e2 | Case e0 e1 e2 | CAS e0 e1 e2 =>
    is_closed X e0 && is_closed X e1 && is_closed X e2
  | Fst e | Snd e | InjL e | InjR e | Fork e | Alloc e
          | Load e  | Fold e | Unfold e | Pack e
          | TLam e | TApp e => is_closed X e
  end.

Fixpoint subst (x : string) (es : expr) (e : expr)  : expr :=
  match e with
  | Val _ _ _ => e
  | ClosedExpr _ => e
  | Var y => if decide (x = y) then es else Var y
  | Rec f y e =>
    Rec f y $ if decide (BNamed x ≠ f ∧ BNamed x ≠ y) then subst x es e else e
  | App e1 e2 => App (subst x es e1) (subst x es e2)
  | TLam e => TLam (subst x es e)
  | TApp e => TApp (subst x es e)
  | Lit l => Lit l
  | BinOp op e1 e2 => BinOp op (subst x es e1) (subst x es e2)
  | If e0 e1 e2 => If (subst x es e0) (subst x es e1) (subst x es e2)
  | Pair e1 e2 => Pair (subst x es e1) (subst x es e2)
  | Fst e => Fst (subst x es e)
  | Snd e => Snd (subst x es e)
  | Fold e => Fold (subst x es e)
  | Unfold e => Unfold (subst x es e)
  | Pack e => Pack (subst x es e)
  | Unpack e0 e =>
    Unpack (subst x es e0) (subst x es e)
  | InjL e => InjL (subst x es e)
  | InjR e => InjR (subst x es e)
  | Case e0 e1 e2 =>
    Case (subst x es e0)
         (subst x es e1)
         (subst x es e2)
  | Fork e => Fork (subst x es e)
  | Alloc e => Alloc (subst x es e)
  | Load e => Load (subst x es e)
  | Store e1 e2 => Store (subst x es e1) (subst x es e2)
  | CAS e0 e1 e2 => CAS (subst x es e0) (subst x es e1) (subst x es e2)
  end.

Lemma is_closed_correct X e : is_closed X e → lang.is_closed X (to_expr e).
Proof.
  revert X.
  induction e; cbn; try naive_solver.
  - intros. rewrite -(of_to_val _ _ Hev). eapply of_val_closed'.
  - intros. eapply is_closed_weaken; eauto. set_solver.
Qed.

(* We define [to_val (ClosedExpr _)] to be [None] since [ClosedExpr]
constructors are only generated for closed expressions of which we know nothing
about apart from being closed. Notice that the reverse implication of
[to_val_Some] thus does not hold. *)
Fixpoint to_val (e : expr) : option val :=
  match e with
  | Val v _ _ => Some v
  | Rec f x e =>
     if decide (is_closed (x :b: f :b: ∅) e) is left H
     then Some (@RecV f x (to_expr e) (is_closed_correct _ _ H)) else None
  | TLam e =>
     if decide (is_closed ∅ e) is left H
     then Some (@TLamV (to_expr e) (is_closed_correct _ _ H)) else None
  | Lit l => Some (LitV l)
  | Pair e1 e2 => v1 ← to_val e1; v2 ← to_val e2; Some (PairV v1 v2)
  | InjL e => InjLV <$> to_val e
  | InjR e => InjRV <$> to_val e
  | Pack e => v ← to_val e; Some (PackV v)
  | Fold e => v ← to_val e; Some (FoldV v)
  | _ => None
  end.

Lemma subst_correct x es e :
  lang.subst x (to_expr es) (to_expr e) = to_expr (subst x es e).
Proof.
  induction e; cbn; try (congruence || naive_solver).
  - rewrite -(of_to_val _ _ Hev).
    by rewrite (@Closed_subst_id _ _ _ (of_val_closed v)).
  - by rewrite (@Closed_subst_id _ _ _ H).
  - case_match; eauto.
  - case_match; eauto. congruence.
Qed.

Lemma to_val_Some e v :
  to_val e = Some v → lang.to_val (to_expr e) = Some v.
Proof.
  revert v. induction e; intros; simplify_option_eq; rewrite ?to_of_val; auto.
  - do 2 f_equal. apply proof_irrel.
  - exfalso. unfold Closed in *; eauto using is_closed_correct.
  - do 2 f_equal. apply proof_irrel.
  - exfalso. unfold Closed in *; eauto using is_closed_correct.
Qed.

Lemma to_val_is_Some e :
  is_Some (to_val e) → is_Some (lang.to_val (to_expr e)).
Proof. intros [v ?]; exists v; eauto using to_val_Some. Qed.
End R.

Ltac solve_to_val :=
  rewrite /IntoVal;
  try match goal with
  | |- context E [language.to_val ?e] =>
     let X := context E [to_val e] in change X
  end;
  match goal with
  | |- to_val ?e = Some ?v =>
     let e' := R.of_expr e in change (to_val (R.to_expr e') = Some v);
     apply R.to_val_Some; simpl; unfold R.to_expr; unlock; reflexivity
  | |- is_Some (to_val ?e) =>
     let e' := R.of_expr e in change (is_Some (to_val (R.to_expr e')));
     apply R.to_val_is_Some, (bool_decide_unpack _); vm_compute; exact I
  end.
Hint Extern 0 (IntoVal _ _) => solve_to_val : typeclass_instances.

(* TODO:
DF: this is also kind of a hack.
Basically, we canno't really solve goals of the form `Closed X e` for an abstract `X`.
`vm_compute` loops on goals like `bool_decide (x ∈ {[x]} ∪ X)`. *)
Ltac pre_solve_closed :=
  lazymatch goal with
  | [ |- Closed ?X ?e ] =>
    apply (Closed_mono e ∅ X); [| apply empty_subseteq]
  end.

Ltac solve_closed :=
  pre_solve_closed;
  match goal with
  | [ |- Closed ?X ?e] =>
     let e' := R.of_expr e in change (is_closed X (R.to_expr e'));
     eapply R.is_closed_correct; vm_compute; exact I
  | [ |- Is_true (is_closed ?X ?e)] =>
     let e' := R.of_expr e in change (is_closed X (R.to_expr e'));
     eapply R.is_closed_correct; vm_compute; exact I
  end.
Hint Extern 0 (Closed _ _) => solve_closed : typeclass_instances.
Hint Extern 0 (Closed _ _) => solve_closed.

(* TODO: This is horrible, but it actually works. *)
(*    DF: *)
(*    The issue here is that some lemmas about Rec's depend on the *)
(*    specific assumption that [Closed (x :b: f :b: ∅) ebody] holds. *)
(*    This duplication leads to confusion and necessitates monstrosities likes this one. *)
Ltac solve_closed2 :=
  lazymatch goal with
  | [ H: Closed ?X (Rec ?f ?x ?e) |- Closed (?x :b: ?f :b: ?X) ?e ] =>
    change (Closed X (Rec f x e)); apply H
  | [ |- Closed (?x :b: ?f :b: ?X) ?e ] =>
    change (Closed X (Rec f x e)); solve_closed
  | [ H: Closed (?x :b: ?f :b: ?X) ?e |- Closed ?X (Rec ?f ?x ?e) ] =>
    change (Closed (x :b: f :b: X) e); apply H
  | _ => solve_closed
  end.
Hint Extern 1 (Closed _ _) => solve_closed2 : typeclass_instances.

Ltac simpl_subst :=
  cbn[subst'];
  repeat match goal with
  | |- context [subst ?x ?er ?e] =>
      let er' := R.of_expr er in let e' := R.of_expr e in
      change (subst x er e) with (subst x (R.to_expr er') (R.to_expr e'));
      rewrite (R.subst_correct x); simpl
  end;
  unfold R.to_expr.
Tactic Notation "simpl_subst/=" := simpl; simpl_subst.

(* TODO: the following breaks a lot of shit *)
(* Arguments subst : simpl never. *)
Arguments R.to_expr : simpl never.

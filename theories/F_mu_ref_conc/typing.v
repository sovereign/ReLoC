From iris_logrel.F_mu_ref_conc Require Export lang notation subst.
From Autosubst Require Import Autosubst_Classes. (* for [subst], we have to import this last *)

(** Types *)
Inductive type :=
  | TUnit : type
  | TNat : type
  | TBool : type
  | TProd : type → type → type
  | TSum : type → type → type
  | TArrow : type → type → type
  | TRec (τ : {bind 1 of type})
  | TVar (x : var)
  | TForall (τ : {bind 1 of type})
  | TExists (τ : {bind 1 of type})
  | Tref (τ : type).

Instance Ids_type : Ids type. derive. Defined.
Instance Rename_type : Rename type. derive. Defined.
Instance Subst_type : Subst type. derive. Defined.
Instance SubstLemmas_typer : SubstLemmas type. derive. Qed.

Fixpoint binop_nat_res_type (op : binop) : option type :=
  match op with
  | Mul => Some TNat | Add => Some TNat | Sub => Some TNat
  | Eq => Some TBool | Le => Some TBool | Lt => Some TBool
  | _ => None
  end.
Fixpoint binop_bool_res_type (op : binop) : option type :=
  match op with
  | Xor => Some TBool | Eq => Some TBool
  | _ => None
  end.

Inductive EqType : type → Prop :=
  | EqTUnit : EqType TUnit
  | EqTNat : EqType TNat
  | EqTBool : EqType TBool
  | EqTProd τ τ' : EqType τ → EqType τ' → EqType (TProd τ τ')
  | EqSum τ τ' : EqType τ → EqType τ' → EqType (TSum τ τ').

Delimit Scope FType_scope with F.
Bind Scope FType_scope with type.
Notation "'Unit'" := TUnit : FType_scope.
Notation "'Bool'" := TBool : FType_scope.
(* TODO: this clash with the module `Nat` *)
(* Notation "'Nat'" := TNat : FType_scope. *)
Infix "×" := TProd : FType_scope.
Notation "(×)" := TProd (only parsing) : FType_scope.
Infix "+" := TSum : FType_scope.
Notation "(+)" := TSum (only parsing) : FType_scope.
Infix "→" := TArrow : FType_scope.
Notation "(→)" := TArrow (only parsing) : FType_scope.
Notation "μ: τ" :=
  (TRec τ%F)
  (at level 100, τ at level 200) : FType_scope.
Notation "∀: τ" :=
  (TForall τ%F)
  (at level 100, τ at level 200) : FType_scope.
Notation "∃: τ" :=
  (TExists τ%F)
  (at level 100, τ at level 200) : FType_scope.
Notation "'ref' τ" := (Tref τ%F) (at level 30, right associativity): FType_scope.

(** Typing judgements *)
Reserved Notation "Γ ⊢ₜ e : τ" (at level 74, e, τ at next level).

Inductive typed (Γ : stringmap type) : expr → type → Prop :=
  | Var_typed x τ : Γ !! x = Some τ → Γ ⊢ₜ Var x : τ
  | Unit_typed : Γ ⊢ₜ #() : TUnit
  | Nat_typed (n : nat) : Γ ⊢ₜ # n : TNat
  | Bool_typed (b : bool) : Γ ⊢ₜ # b : TBool
  | BinOp_typed_nat op e1 e2 τ :
     Γ ⊢ₜ e1 : TNat → Γ ⊢ₜ e2 : TNat → 
     binop_nat_res_type op = Some τ →
     Γ ⊢ₜ BinOp op e1 e2 : τ
  | BinOp_typed_bool op e1 e2 τ :
     Γ ⊢ₜ e1 : TBool → Γ ⊢ₜ e2 : TBool → 
     binop_bool_res_type op = Some τ →
     Γ ⊢ₜ BinOp op e1 e2 : τ
  | RefEq_typed e1 e2 τ :
     Γ ⊢ₜ e1 : Tref τ → Γ ⊢ₜ e2 : Tref τ →
     Γ ⊢ₜ BinOp Eq e1 e2 : TBool
  | Pair_typed e1 e2 τ1 τ2 : Γ ⊢ₜ e1 : τ1 → Γ ⊢ₜ e2 : τ2 → Γ ⊢ₜ Pair e1 e2 : TProd τ1 τ2
  | Fst_typed e τ1 τ2 : Γ ⊢ₜ e : TProd τ1 τ2 → Γ ⊢ₜ Fst e : τ1
  | Snd_typed e τ1 τ2 : Γ ⊢ₜ e : TProd τ1 τ2 → Γ ⊢ₜ Snd e : τ2
  | InjL_typed e τ1 τ2 : Γ ⊢ₜ e : τ1 → Γ ⊢ₜ InjL e : TSum τ1 τ2
  | InjR_typed e τ1 τ2 : Γ ⊢ₜ e : τ2 → Γ ⊢ₜ InjR e : TSum τ1 τ2
  | Case_typed e0 e1 e2 τ1 τ2 τ3 :
     Γ ⊢ₜ e0 : TSum τ1 τ2 → 
     Γ ⊢ₜ e1 : TArrow τ1 τ3 →
     Γ ⊢ₜ e2 : TArrow τ2 τ3 →
     Γ ⊢ₜ Case e0 e1 e2 : τ3
  | If_typed e0 e1 e2 τ :
     Γ ⊢ₜ e0 : TBool → Γ ⊢ₜ e1 : τ → Γ ⊢ₜ e2 : τ → Γ ⊢ₜ If e0 e1 e2 : τ
  | Rec_typed f x e τ1 τ2 :
     <[x:=τ1]>(<[f:=TArrow τ1 τ2]>Γ) ⊢ₜ e : τ2 →
     Γ ⊢ₜ Rec f x e : TArrow τ1 τ2
  | App_typed e1 e2 τ1 τ2 :
     Γ ⊢ₜ e1 : TArrow τ1 τ2 → Γ ⊢ₜ e2 : τ1 → Γ ⊢ₜ App e1 e2 : τ2
  | TLam_typed e τ :
     subst (ren (+1)) <$> Γ ⊢ₜ e : τ → Γ ⊢ₜ TLam e : TForall τ
  | TApp_typed e τ τ' : Γ ⊢ₜ e : TForall τ → Γ ⊢ₜ TApp e : τ.[τ'/]
  | TFold e τ : Γ ⊢ₜ e : τ.[TRec τ/] → Γ ⊢ₜ Fold e : TRec τ
  | TUnfold e τ : Γ ⊢ₜ e : TRec τ → Γ ⊢ₜ Unfold e : τ.[TRec τ/]
  | TPack e τ τ' : Γ ⊢ₜ e : τ.[τ'/] → Γ ⊢ₜ Pack e : TExists τ
  | TUnpack e1 e2 τ τ2 : Γ ⊢ₜ e1 : TExists τ →
      (subst (ren (+1)) <$> Γ) ⊢ₜ e2 : TArrow τ (subst (ren (+1)) τ2) →
      Γ ⊢ₜ Unpack e1 e2 : τ2
  | TFork e : Γ ⊢ₜ e : TUnit → Γ ⊢ₜ Fork e : TUnit
  | TAlloc e τ : Γ ⊢ₜ e : τ → Γ ⊢ₜ Alloc e : Tref τ
  | TLoad e τ : Γ ⊢ₜ e : Tref τ → Γ ⊢ₜ Load e : τ
  | TStore e e' τ : Γ ⊢ₜ e : Tref τ → Γ ⊢ₜ e' : τ → Γ ⊢ₜ Store e e' : TUnit
  | TCAS e1 e2 e3 τ :
     EqType τ → Γ ⊢ₜ e1 : Tref τ → Γ ⊢ₜ e2 : τ → Γ ⊢ₜ e3 : τ →
     Γ ⊢ₜ CAS e1 e2 e3 : TBool
where "Γ ⊢ₜ e : τ" := (typed Γ e τ).

(** A hint db for the typing information *)

Create HintDb typeable.

Hint Constructors typed : typeable.

(** we need to replace some of the constructors with lemmas better suitable for search *)
Lemma TCAS' Γ e1 e2 e3 τ :
  Γ ⊢ₜ e1 : Tref τ → Γ ⊢ₜ e2 : τ → Γ ⊢ₜ e3 : τ →
  EqType τ → 
  Γ ⊢ₜ CAS e1 e2 e3 : Bool.
Proof. eauto using TCAS. Qed.

Hint Resolve TCAS' : typeable.
Remove Hints TCAS : typeable.

Lemma TUNFOLD' Γ e τ τ' :
  Γ ⊢ₜ e : (μ: τ)%F →
  τ' = τ.[(μ: τ)%F/] →
  Γ ⊢ₜ Unfold e : τ'.
Proof. intros. subst τ'. by econstructor. Qed.

Hint Resolve TUNFOLD' : typeable.
Remove Hints TUnfold : typeable.

Hint Constructors EqType : typeable.

Hint Extern 10 (<[_:=_]>_ !! _ = Some _) => eapply lookup_insert : typeable.
Hint Extern 20 (<[_:=_]>_ !! _ = Some _) => rewrite lookup_insert_ne; last done : typeable.

Lemma locked_val_typed Γ (v : val) (τ : type) :
  Γ ⊢ₜ of_val v : τ →
  Γ ⊢ₜ of_val (locked v) : τ.
Proof. by unlock. Qed.

Tactic Notation "solve_typed" int_or_var(n) :=
  try apply locked_val_typed; eauto n with typeable.

Tactic Notation "solve_typed" := solve_typed 50.


(** Typeability of binops *)
Lemma binop_nat_typed_safe (op : binop) (n1 n2 : nat) τ :
  binop_nat_res_type op = Some τ → is_Some (binop_eval op #n1 #n2).
Proof.
  destruct op; simpl; eauto.
  inversion 1.
Qed.

Lemma binop_bool_typed_safe (op : binop) (b1 b2 : bool) τ :
  binop_bool_res_type op = Some τ → is_Some (binop_eval op #b1 #b2).
Proof.
  destruct op; simpl; eauto; try by inversion 1.
Qed.

(** Environment substitution and closedness *)
Definition env_subst := subst_p. 

Lemma env_subst_empty (e : expr) : env_subst ∅ e = e.
Proof. exact: subst_p_empty. Qed.

Lemma env_subst_lookup_None (vs : stringmap val) (x : string) :
  vs !! x = None →
   env_subst vs (Var x) = Var x.
Proof.
  intro H; simpl. by rewrite H.
Qed.

Lemma env_subst_lookup (vs : stringmap val) (x : string) (v : val) :
  vs !! x = Some v →
  env_subst vs (Var x) = of_val v.
Proof.
  intro H; simpl; by rewrite H.
Qed.
  
Lemma typed_X_closed Γ τ e :
  Γ ⊢ₜ e : τ → Closed (dom (stringset) Γ) e.
Proof.
  intros H. rewrite /Closed. induction H => /=; simpl in *; auto with f_equal.
  - case_bool_decide; auto.
    apply H0. 
    rewrite elem_of_dom. by exists τ.
  - revert IHtyped.
    rewrite !dom_insert_binder.
    destruct f, x; cbn-[union];
    rewrite ?(left_id ∅ union); eauto.
  - rewrite -dom_fmap. eassumption.
  - split_and?; [ | rewrite -dom_fmap ]; eauto.
Qed.

(** Weakening *)
Lemma context_gen_weakening Γ Δ e τ :
  Γ ⊆ Δ →
  Γ ⊢ₜ e : τ →
  Δ ⊢ₜ e : τ.
Proof.
  intros Hsub Ht. revert Hsub. generalize dependent Δ.
  induction Ht => Δ Hsub; subst; eauto with typeable.
  - econstructor. by eapply lookup_weaken.
  - econstructor. eapply IHHt.    
    destruct f, x; cbn; eauto;
    repeat eapply insert_mono; done.
  - econstructor. eapply IHHt.
    by apply map_fmap_mono.
  - econstructor.
    * by eapply IHHt1.
    * eapply IHHt2. by apply map_fmap_mono.
Qed.

(* Type synonyms *)
Notation MAYBE τ := (Unit + τ)%F (only parsing).

From iris_logrel Require Export logrel.
From iris.proofmode Require Import tactics.

Section contents.
Context `{logrelG Σ}.
Lemma ex Δ Γ :
  {Δ;Γ} ⊨ (λ: "x", "x" <- #1)
              ≤log≤
              (λ: "x", Fork ("x" <- #1);; Fork ("x" <- #0)) : TArrow (Tref TNat) TUnit.
Proof.
  iApply bin_log_related_arrow_val; eauto.
  iAlways. iIntros (? ?) "Hx".
  simpl. iDestruct "Hx" as ([x1 x2]) "[% #Hx]"; simplify_eq/=.
  rel_let_l. rel_let_r.
  rel_store_l_atomic.
  iInv (logN.@(x1,x2)) as ([v1 v2]) "(Hx1 & Hx2 & Hvs)" "Hcl".
  iModIntro. iExists _. iFrame. iNext. iIntros "Hx1".
  rel_fork_r as i "Hi". rel_seq_r.
  rel_fork_r as j "Hj".
  apply bin_log_related_spec_ctx. iDestruct 1 as (?) "?".
  tp_store i.
  iMod ("Hcl" with "[-]") as "_".
  { iNext. iExists (_,_); iFrame. iExists _; eauto. }
  iApply bin_log_related_unit.
Qed.
End contents.

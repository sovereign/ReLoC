From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris.program_logic Require Import hoare.
From iris_logrel Require Import examples.counter.

Section liftings.
  Context `{logrelG Σ}.

  (* left-side hoare triples *)
  Definition lhs_ht (P : iProp Σ) (e : expr) (Q : val → iProp Σ) Δ Γ :=
    (∀ K t τ, □(P -∗ (∀ v, Q v -∗ {Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ)
        -∗ {Δ;Γ} ⊨ fill K e ≤log≤ t : τ))%I.

  Lemma lift_ht (P : iProp Σ) (e : expr) (Q : val → iProp Σ) E Δ Γ `{Closed ∅ e} :
    ({{ P }} e @ E {{ Q }}
     → lhs_ht P e Q Δ Γ)%I.
  Proof.
    iIntros "#He" (K t τ).
    iAlways. iIntros "HP"; iSpecialize ("He" with "HP").
    iIntros "Hlog".
    iApply bin_log_related_wp_l.
    iApply (wp_mask_mono _ E); first done.
    by iApply (wp_wand with "He").
  Qed.

  (* Definition taken from iris-atomic, strengthened a bit *)
  Definition atomic_triple {A : Type}
             (α: A → iProp Σ) (* atomic pre-condition *)
             (β: A → val → iProp Σ) (* atomic post-condition *)
             (Ei Eo: coPset) (* inside/outside masks *)
             (e: expr) : iProp Σ :=
    (∀ P Q, (P ={Eo, Ei}=> ∃ x:A,
               α x ∗
                 ((α x ={Ei, Eo}=∗ P) ∧
                  (∀ v, β x v ={Ei, Eo}=∗ Q v))
            ) -∗ {{ P }} e @ Eo {{ Q }})%I.

  (* left-side logically atomic triples *)
  (* Eo = E1, Ei = E2 *)
  (* TODO MASKS, outside mask is now always Top *)
  Definition atomic_logrel {A : Type}
             (α: A → iProp Σ) (* atomic pre-condition *)
             (β: A → val → iProp Σ) (* atomic post-condition *)
             (Ei : coPset) (* inside/outside masks *)
             (e: expr)
             Δ Γ : iProp Σ :=
    (∀ K t τ R2 R1, (R2 ∗ □ (|={⊤,Ei}=> ∃ (x : A),
                 α x ∗ R1 x ∗
                 (((∃ x, α x ∗ R1 x) ={Ei, ⊤}=∗ True) ∧
                  (∀ y v, β y v ∗ R1 y ∗ R2 -∗ {Ei;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ)))
            ) -∗ {Δ;Γ} ⊨ fill K e ≤log≤ t : τ)%I.

  (* We can prove the atomic specification for the counter *)
  Lemma counter_atomic x E Δ Γ :
    atomic_logrel
      (fun (n : nat) => x ↦ᵢ #n)%I
      (fun (n : nat) (v : val) => ⌜v = #n⌝ ∗ x ↦ᵢ #(S n))%I
      E
      (FG_increment #x)
      Δ Γ.
  Proof.
    iIntros (K t τ R2 R1) "[HR2 #Hlog]".
    iApply (bin_log_FG_increment_logatomic _ R1 with "HR2").
    iAlways. iMod "Hlog" as (n) "(Hx & HR & Hlog)".
    iModIntro. iExists _. iFrame.
    iSplit.
    - iDestruct "Hlog" as "[Hlog _]". done.
    - iDestruct "Hlog" as "[_ Hlog]".
      iIntros (m) "[Hx HR1] HR2".
      iApply ("Hlog" $! m #m). by iFrame.
  Qed.

  Lemma LA_lift_wtf {A : Type} α (e : expr) β Δ Γ :
    (∀ (x : A), lhs_ht (α x) e (β x) Δ Γ) -∗
    atomic_logrel α β ⊤ e Δ Γ.
  Proof.
    rewrite /atomic_logrel.
    iIntros "#HT" (K t τ).
    iIntros (R2 R1) "[HR2 #Hlog]".
    iMod "Hlog" as (x) "(Hα & HR & Hm)".
    iApply ("HT" $! x K t τ with "Hα").
    iIntros (v) "Hβ".
    iDestruct "Hm" as "[_ Hm]".
    iApply "Hm". by iFrame.
  Qed.

End liftings.

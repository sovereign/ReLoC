From iris.proofmode Require Import tactics.
From iris_logrel.F_mu_ref_conc Require Export tactics notation.

Lemma wp_rec1 `{heapG Σ} Φ :
  ▷ Φ #4
  ⊢ WP (λ: "y", (λ: "x", "x" + #3) "y") #1 {{ Φ }}.
Proof.
  iIntros "Z".
  wp_rec.
  wp_rec.
  by wp_op.
Qed.

Lemma wp_proj2 `{heapG Σ} Φ :
  ▷ Φ #1
  ⊢ WP Fst (Fst (#1,#2,#4)) {{ Φ }}.
Proof.
  iIntros "HΦ".
  wp_proj.
  by wp_proj.
Qed.

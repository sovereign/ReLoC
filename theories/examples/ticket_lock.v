From iris.proofmode Require Import tactics.
From iris.algebra Require Export auth gset excl.
From iris.base_logic Require Import auth.
From iris_logrel Require Export logrel examples.lock examples.counter.

Definition wait_loop: val :=
  rec: "wait_loop" "x" "lk" :=
    if: "x" = !(Fst "lk")
      then #() (* my turn *)
      else "wait_loop" "x" "lk".

Definition newlock : val :=
  λ: <>, ((* owner *) ref #0, (* next *) ref #0).

Definition acquire : val := λ: "lk",
  let: "n" := FG_increment (Snd "lk") in
  wait_loop "n" "lk".

Definition wkincr : val := λ: "l",
  "l" <- !"l" + #1.
Definition release : val := λ: "lk", wkincr (Fst "lk").

Definition LockType : type := ref TNat × ref TNat.

Hint Unfold LockType : typeable.

Lemma newlock_type Γ : typed Γ newlock (Unit → LockType).
Proof. solve_typed. Qed.

Hint Resolve newlock_type : typeable.

Lemma acquire_type Γ : typed Γ acquire (LockType → TUnit).
Proof.
  unlock acquire wait_loop.
  econstructor; cbn; solve_typed.
  econstructor; cbn; solve_typed.
  econstructor; cbn; solve_typed.
Qed.

Hint Resolve acquire_type : typeable.

Lemma release_type Γ : typed Γ release (LockType → TUnit).
Proof. unlock release wkincr. solve_typed. Qed.

Hint Resolve release_type : typeable.

Definition lockT : type := ∃: (Unit → TVar 0) × (TVar 0 → Unit) × (TVar 0 → Unit).
Lemma ticket_lock_typed Γ : typed Γ (Pack (newlock, acquire, release)) lockT.
Proof.
  apply TPack with LockType.
  asimpl. solve_typed.
Qed.

Class tlockG Σ :=
  tlock_G :> authG Σ (gset_disjUR nat).
Definition tlockΣ : gFunctors :=
  #[ authΣ (gset_disjUR nat) ].

Definition lockPool := gset ((loc * loc * gname) * loc).
Definition lockPoolR := gsetUR ((loc * loc * gname) * loc).

Class lockPoolG Σ :=
  lockPool_inG :> authG Σ lockPoolR.
Section refinement.
  Context `{logrelG Σ, tlockG Σ, lockPoolG Σ}.

  (** * Basic abstractions around the concrete RA *)

  (** ticket with the id `n` *)
  Definition ticket (γ : gname) (n : nat) := own γ (◯ GSet {[ n ]}).
  (** total number of issued tickets is `n` *)
  Definition issuedTickets (γ : gname) (n : nat) := own γ (● GSet (seq_set 0 n)).

  Lemma ticket_nondup γ n : ticket γ n -∗ ticket γ n -∗ False.
  Proof.
    iIntros "Ht1 Ht2".
    iDestruct (own_valid_2 with "Ht1 Ht2") as %?%gset_disj_valid_op.
    set_solver.
  Qed.

  Lemma newIssuedTickets : (|==> ∃ γ, issuedTickets γ 0)%I.
  Proof. iMod (own_alloc (● (GSet ∅))) as (γ) "Hγ"; [done|eauto]. Qed.

  Lemma issueNewTicket γ m :
    issuedTickets γ m ==∗
    issuedTickets γ (S m) ∗ ticket γ m.
  Proof.
    iIntros "Hseq".
    iMod (own_update with "Hseq") as "[Hseq Hticket]".
    { eapply auth_update_alloc.
      eapply (gset_disj_alloc_empty_local_update _ {[ m ]}).
      apply (seq_set_S_disjoint 0). }
    rewrite -(seq_set_S_union_L 0).
    by iFrame.
  Qed.

  Instance ticket_timeless γ n : Timeless (ticket γ n).
  Proof. apply _. Qed.
  Instance issuedTickets_timeless γ n : Timeless (issuedTickets γ n).
  Proof. apply _. Qed.

  Opaque ticket issuedTickets.

  (** * Invariants and abstracts for them *)
  Definition lockInv (lo ln : loc) (γ : gname) (l' : loc) : iProp Σ :=
    (∃ (o n : nat) (b : bool), lo ↦ᵢ #o ∗ ln ↦ᵢ #n
   ∗ issuedTickets γ n ∗ l' ↦ₛ #b
   ∗ if b then ticket γ o else True)%I.

  Instance ifticket_timeless (b : bool) γ o : Timeless (if b then ticket γ o else True%I).
  Proof. destruct b; apply _. Qed.
  Instance lockInv_timeless lo ln γ l' : Timeless (lockInv lo ln γ l').
  Proof. apply _. Qed.

  Definition N := logrelN.@"locked".

  Program Definition lockInt := λne vv,
    (∃ (lo ln : loc) (γ : gname) (l' : loc),
        ⌜vv.1 = (#lo, #ln)%V⌝ ∗ ⌜vv.2 = #l'⌝
      ∗ inv N (lockInv lo ln γ l'))%I.
  Next Obligation. solve_proper. Qed.
  
  Instance lockInt_persistent ww : Persistent (lockInt ww).
  Proof. apply _. Qed.

  (** * Refinement proofs *)

  Local Ltac openI :=
    iInv N as (o n b) ">(Hlo & Hln & Hissued & Hl' & Hbticket)" "Hcl".
  Local Ltac closeI := iMod ("Hcl" with "[-]") as "_";
    first by (iNext; iExists _,_,_; iFrame).

  (* Allocating a new lock *)
  Lemma newlock_refinement Δ Γ:
    {(lockInt:: Δ); ⤉Γ} ⊨ newlock ≤log≤ lock.newlock : (Unit → TVar 0).
  Proof.
    unlock newlock.
    iApply bin_log_related_arrow_val; eauto.
    { by unlock lock.newlock. }
    iAlways. iIntros (? ?) "/= [% %]"; simplify_eq.
    (* Reducing to a value on the LHS *)
    rel_let_l.
    rel_alloc_l as lo "Hlo".
    rel_alloc_l as ln "Hln".
    (* Reducing to a value on the RHS *)
    rel_apply_r bin_log_related_newlock_r.
    { solve_ndisj. }
    iIntros (l') "Hl'".
    (* Establishing the invariant *)
    iMod newIssuedTickets as (γ) "Hγ".
    iMod (inv_alloc N _ (lockInv lo ln γ l') with "[-]") as "#Hinv".
    { iNext. iExists _,_,_. iFrame. }
    rel_vals. iModIntro. iAlways.
    iExists _,_,_,_. iFrame "Hinv". eauto.
  Qed.

  (* Acquiring a lock *)
  (* helper lemma *)
  Lemma wait_loop_refinement Δ Γ (lo ln : loc) γ (l' : loc) (m : nat) :
    inv N (lockInv lo ln γ l') -∗
    ticket γ m -∗
    {(lockInt :: Δ); ⤉Γ} ⊨
      wait_loop #m (#lo, #ln) ≤log≤ lock.acquire #l' : TUnit.
  Proof.
    iIntros "#Hinv Hticket".
    rel_rec_l.
    iLöb as "IH".
    unlock {2}wait_loop. simpl.
    rel_let_l. rel_proj_l.
    rel_load_l_atomic.
    openI.
    iModIntro. iExists _; iFrame; iNext.
    iIntros "Hlo".
    rel_op_l.
    case_decide; subst; rel_if_l.
    (* Whether the ticket is called out *)
    - destruct b.
      { iDestruct (ticket_nondup with "Hticket Hbticket") as %[]. }
      rel_apply_r (bin_log_related_acquire_r with "Hl'").
      { solve_ndisj. }
      iIntros "Hl'".
      closeI.
      iApply bin_log_related_unit.
    - iMod ("Hcl" with "[-Hticket]") as "_".
      { iNext. iExists _,_,_; by iFrame. }
      rel_rec_l.
      unlock wait_loop. simpl_subst/=. by iApply "IH".
  Qed.

  (** Logically atomic spec for `acquire`.
      Parameter type: nat
      Precondition:
        λo, ∃ n, lo ↦ᵢ o ∗ ln ↦ᵢ n ∗ issuedTickets γ n
      Postcondition:
        λo, ∃ n, lo ↦ᵢ o ∗ ln ↦ᵢ n ∗ issuedTickets γ n ∗ ticket γ o *)
  Lemma acquire_l_logatomic R P γ Δ Γ E K lo ln t τ :
    P -∗
    □ (|={⊤,E}=> ∃ o n : nat, lo ↦ᵢ #o ∗ ln ↦ᵢ #n ∗ issuedTickets γ n ∗ R o ∗
       ((∃ o n : nat, lo ↦ᵢ #o ∗ ln ↦ᵢ #n ∗ issuedTickets γ n ∗ R o) ={E,⊤}=∗ True) ∧
        (∀ o n : nat, lo ↦ᵢ #o ∗ ln ↦ᵢ #n ∗ issuedTickets γ n ∗ ticket γ o ∗ R o -∗ P -∗
            {E;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ))
    -∗ ({Δ;Γ} ⊨ fill K (acquire (#lo, #ln)) ≤log≤ t : τ).
  Proof.
    iIntros "HP #H".
    rewrite /acquire. unlock. simpl.
    rel_rec_l. rel_proj_l.
    rel_apply_l (bin_log_FG_increment_logatomic _ (fun n : nat => ∃ o : nat, lo ↦ᵢ #o ∗ issuedTickets γ n ∗ R o)%I P%I with "HP").
    iAlways.
    iPoseProof "H" as "H2".
    iMod "H" as (o n) "(Hlo & Hln & Hissued & HR & Hrest)". iModIntro.
    iExists _; iFrame.
    iSplitL "Hlo HR".
    { iExists _. iFrame. }
    iSplit.
    - iDestruct "Hrest" as "[H _]".
      iDestruct 1 as (n') "[Hln Ho]".
      iDestruct "Ho" as (o') "[Ho HR]".
      iApply "H".
      iExists _, _. iFrame.
    - iDestruct "Hrest" as "[H _]".
      iIntros (n') "[Hln Ho] HP".
      iDestruct "Ho" as (o') "[Ho [Hissued HR]]".
      iMod (issueNewTicket with "Hissued") as "[Hissued Hm]".
      iMod ("H" with "[-HP Hm]") as "_".
      { iExists _,_. iFrame. }
      rel_let_l. clear o n o'.
      rel_rec_l.
      iLöb as "IH".
      unlock wait_loop. simpl.
      rel_rec_l. rel_proj_l.
      rel_load_l_atomic.
      iMod "H2" as (o n) "(Hlo & Hln & Hissued & HR & Hrest)". iModIntro.
      iExists _. iFrame. iNext. iIntros "Hlo".
      rel_op_l.
      case_decide; subst; rel_if_l.
      (* Whether the ticket is called out *)
      + iDestruct "Hrest" as "[_ H]".
        iApply ("H" with "[-HP] HP").
        { iFrame. }
      + iDestruct "Hrest" as "[H _]".
        iMod ("H" with "[-HP Hm]") as "_".
        { iExists _,_; iFrame. }
        rel_rec_l. iApply ("IH" with "HP Hm").
  Qed.

  Lemma acquire_refinement Δ Γ :
    {lockInt :: Δ; ⤉Γ} ⊨ acquire ≤log≤ lock.acquire : (TVar 0 → Unit).
  Proof.
    iApply bin_log_related_arrow_val; eauto.
    { by unlock acquire. }
    { by unlock lock.acquire. }
    iAlways. iIntros (? ?) "/= #Hl".
    iDestruct "Hl" as (lo ln γ l') "(% & % & Hin)". simplify_eq/=.
    rel_apply_l (acquire_l_logatomic
                   (fun o => ∃ (b : bool),
                             l' ↦ₛ #b ∗
                             if b then ticket γ o else True)%I
                   True%I γ); first done.
    iAlways.
    openI.
    iModIntro. iExists _,_; iFrame.
    iSplitL "Hbticket Hl'".
    { iExists _. iFrame. }
    clear b o n.
    iSplit.
    - iDestruct 1 as (o' n') "(Hlo & Hln & Hissued & Hrest)".
      iDestruct "Hrest" as (b) "[Hl' Ht]".
      iApply ("Hcl" with "[-]").
      iNext. iExists _,_,_. by iFrame.
    - iIntros (o n) "(Hlo & Hln & Hissued & Ht & Hrest) _".
      iDestruct "Hrest" as (b) "[Hl' Ht']".
      destruct b.
      { iDestruct (ticket_nondup with "Ht Ht'") as %[]. }
      rel_apply_r (bin_log_related_acquire_r with "Hl'").
      { solve_ndisj. }
      iIntros "Hl'".
      iMod ("Hcl" with "[-]") as "_".
      { iNext. iExists _,_,_; by iFrame. }
      iApply bin_log_related_unit.
  Qed.

  Lemma acquire_refinement_direct Δ Γ :
    {(lockInt :: Δ); ⤉Γ} ⊨ acquire ≤log≤ lock.acquire : (TVar 0 → Unit).
  Proof.
    unlock acquire; simpl.
    iApply bin_log_related_arrow_val; eauto.
    { by unlock lock.acquire. }
    iAlways. iIntros (? ?) "/= #Hl".
    iDestruct "Hl" as (lo ln γ l') "(% & % & Hin)". simplify_eq.
    rel_let_l. repeat rel_proj_l.
    rel_apply_l (bin_log_FG_increment_logatomic _ (issuedTickets γ)%I True%I); first done.
    iAlways.
    openI.
    iModIntro. iExists _; iFrame.
    iSplit.
    - iDestruct 1 as (m) "[Hln ?]".
      iApply ("Hcl" with "[-]").
      iNext. iExists _,_,_; by iFrame.
    - iIntros (m) "[Hln Hissued] _".
      iMod (issueNewTicket with "Hissued") as "[Hissued Hm]".
      iMod ("Hcl" with "[-Hm]") as "_".
      { iNext. iExists _,_,_; by iFrame. }
      rel_let_l.
      by iApply wait_loop_refinement.
  Qed.

  (* Releasing the lock *)
  Lemma wkincr_l x (n : nat) Δ Γ K t τ :
    x ↦ᵢ #n -∗
    (x ↦ᵢ #(n+1) -∗ {Δ;Γ} ⊨ fill K Unit ≤log≤ t : τ) -∗
    ({Δ;Γ} ⊨ fill K (wkincr #x) ≤log≤ t : τ).
  Proof.
    iIntros "Hx Hlog".
    unlock wkincr. rel_rec_l.
    rel_load_l. rel_op_l. rel_store_l.
    by iApply "Hlog".
  Qed.

  (* Logically atomic specification for wkincr,
     cf wkIncr specification from (da Rocha Pinto, Dinsdale-Young, Gardner)
     Parameter type: nat
     Precondition: λn, x ↦ᵢ n
     Postcondition λ_, ∃ n, x ↦ᵢ n
 *)
  Lemma wkincr_atomic_l R1 R2 Δ Γ E K x t τ  :
    R2 -∗
    □ (|={⊤,E}=> ∃ n : nat, x ↦ᵢ #n ∗ R1 n ∗
       ((∃ n : nat, x ↦ᵢ #n ∗ R1 n) ={E,⊤}=∗ True) ∧
        (∀ m, (∃ n : nat, x ↦ᵢ #n) ∗ R1 m -∗ R2 -∗
            {E;Δ;Γ} ⊨ fill K Unit ≤log≤ t : τ))
    -∗ ({Δ;Γ} ⊨ fill K (wkincr #x) ≤log≤ t : τ).
  Proof.
    iIntros "HR2 #H".
    unlock wkincr.
    rel_rec_l.
    iPoseProof "H" as "H2".
    rel_load_l_atomic.
    iMod "H" as (n) "[Hx [HR1 [Hrev _]]]". iModIntro.
    iExists _; iFrame. iNext. iIntros "Hx".
    iMod ("Hrev" with "[HR1 Hx]") as "_"; simpl.
    { iExists _. iFrame. }
    rel_op_l.
    rel_store_l_atomic.
    iMod "H2" as (m) "[Hx [HR1 [_ Hmod]]]". iModIntro.
    iExists _; iFrame. iNext. iIntros "Hx".
    iApply ("Hmod" with "[$HR1 Hx] HR2").
    iExists _; iFrame.
  Qed.

  Lemma release_refinement Δ Γ :
    {(lockInt :: Δ); ⤉Γ} ⊨ release ≤log≤ lock.release : (TVar 0 → Unit).
  Proof.
    unlock release.
    iApply bin_log_related_arrow_val; eauto.
    { by unlock lock.release. }
    iAlways. iIntros (? ?) "/= #Hl".
    iDestruct "Hl" as (lo ln γ l') "(% & % & Hin)". simplify_eq.
    rel_let_l. rel_proj_l.
    pose (R := fun (o : nat) =>
                 (∃ (n : nat) (b : bool), ln ↦ᵢ #n
                 ∗ issuedTickets γ n ∗ l' ↦ₛ #b
                 ∗ if b then ticket γ o else True)%I).
    rel_apply_l (wkincr_atomic_l R True%I); first done.
    iAlways.
    openI.
    iModIntro. iExists _; iFrame.
    rewrite {1}/R. iSplitR "Hcl".
    { iExists _,_; by iFrame. } clear o n b.
    iSplit.
    - iDestruct 1 as (o) "[Hlo HR]".
      unfold R. iDestruct "HR" as (n b) "HR".
      iApply "Hcl".
      iNext. iExists _,_,_; by iFrame.
    - iIntros (?) "[Hlo HR] _".
      iDestruct "Hlo" as (o) "Hlo".
      unfold R. iDestruct "HR" as (n b) "(Hln & Hissued & Hl' & Hticket)".
      rel_apply_r (bin_log_related_release_r with "Hl'").
      { solve_ndisj. }
      iIntros "Hl'".
      iMod ("Hcl" with "[-]") as "_".
      { iNext. iExists _,_,_. by iFrame. }
      iApply bin_log_related_unit.
  Qed.

  Lemma ticket_lock_refinement Γ :
    Γ ⊨ Pack (newlock, acquire, release)
      ≤log≤
        Pack (lock.newlock, lock.acquire, lock.release) : lockT.
  Proof.
    iIntros (Δ).
    iApply (bin_log_related_pack lockInt).
    repeat iApply bin_log_related_pair.
    - by iApply newlock_refinement.
    - by iApply acquire_refinement_direct.
    - by iApply release_refinement.
  Qed.

End refinement.

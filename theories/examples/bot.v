From iris.proofmode Require Import tactics.
From iris_logrel Require Export logrel.

Definition bot : val := rec: "bot" <> := "bot" #().

Lemma bot_typed Γ τ :
  Γ ⊢ₜ bot : TArrow TUnit τ.
Proof. solve_typed. Qed.
Hint Resolve bot_typed : typeable.

Section contents.
  Context `{logrelG Σ}.
  Lemma bot_l Δ Γ K t τ :
    {Δ;Γ} ⊨ fill K (bot #()) ≤log≤ t : τ.
  Proof.
    iLöb as "IH".
    rel_rec_l.
    unlock bot; simpl_subst/=.
    iApply "IH".
  Qed.
End contents.

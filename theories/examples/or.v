(* (In)equational theory of erratic choice.

   We can simulate (binary) non-determinism with concurrency. In this
   file we derive the algebraic laws for parallel "or"/demonic choice
   combinator. In particular, we show the following (≤ stands for
   contextual refinement and ≃ stands for contextual equivalence):

   - v or v ≃ v ()           idempotency
   - v1 or v2 ≃ v2 or v1     commutativity
   - v or (λ_, ⊥) ≃ v ()     ⊥ is a unit
   - v1 or (λ_, v2 or v3)     associativity
     ≃ (λ_, v1 or v2) or v3
   - v1 () ≤ v1 or v2        choice on the RHS

    where every v_i is well-typed Unit -> Unit
*)
From iris.proofmode Require Import tactics.
From iris_logrel Require Export logrel examples.bot.

Definition or : val := λ: "e1" "e2",
  let: "x" := ref #0 in
  Fork ("x" <- #1);;
  if: !"x" = #0
  then "e1" #()
  else "e2" #().

Lemma or_type Γ :
  typed Γ or
        (TArrow (TArrow TUnit TUnit) (TArrow (TArrow TUnit TUnit)
            TUnit)).
Proof. solve_typed. Qed.
Hint Resolve or_type : typeable.

Section contents.
  Context `{logrelG Σ}.

  Lemma bin_log_related_or Δ Γ e1 e2 e1' e2' :
    {Δ;Γ} ⊨ e1 ≤log≤ e1' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ e2 ≤log≤ e2' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ or e1 e2 ≤log≤ or e1' e2' : TUnit.
  Proof.
    iIntros "He1 He2".
    iApply (bin_log_related_app with "[He1] He2").
    iApply (bin_log_related_app with "[] He1").
    iApply binary_fundamental; eauto with typeable.
  Qed.

  Lemma bin_log_or_choice_1_r_val Δ Γ (v1 v1' v2 : val) :
    {Δ;Γ} ⊨ v1 ≤log≤ v1' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v1 #() ≤log≤ or v1' v2 : TUnit.
  Proof.
    iIntros "Hlog".
    unlock or. repeat rel_rec_r.
    rel_alloc_r as x "Hx".
    repeat rel_let_r.
    rel_fork_r as j "Hj". rel_seq_r.
    rel_load_r. repeat (rel_pure_r _).
    iApply (bin_log_related_app with "Hlog").
    iApply bin_log_related_unit.
  Qed.

  Lemma bin_log_or_choice_1_r_val_typed Δ Γ (v1 v2 : val) :
    Γ ⊢ₜ v1 : TArrow TUnit TUnit →
    {Δ;Γ} ⊨ v1 #() ≤log≤ or v1 v2 : TUnit.
  Proof.
    iIntros (?).
    iApply bin_log_or_choice_1_r_val; eauto.
    iApply binary_fundamental; eauto with typeable.
  Qed.

  Lemma bin_log_or_choice_1_r Δ Γ (e1 e1' : expr) (v2 : val) :
    {Δ;Γ} ⊨ e1 ≤log≤ e1' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ e1 #() ≤log≤ or e1' v2 : TUnit.
  Proof.
    iIntros "Hlog".
    rel_bind_l e1.
    rel_bind_r e1'.
    iApply (related_bind with "Hlog").
    iIntros ([f f']) "#Hf /=".
    iApply bin_log_or_choice_1_r_val; eauto.
    iApply (related_ret ⊤).
    iApply interp_ret; eauto using to_of_val.
  Qed.

  Lemma bin_log_or_choice_1_r_body Δ Γ (e1 : expr) (v2 : val) :
    Closed ∅ e1 →
    Γ ⊢ₜ e1 : TUnit →
    {Δ;Γ} ⊨ e1 ≤log≤ or (λ: <>, e1) v2 : TUnit.
  Proof.
    iIntros (??).
    unlock or. repeat rel_rec_r.
    rel_alloc_r as x "Hx".
    repeat rel_let_r.
    rel_fork_r as j "Hj". rel_seq_r.
    rel_load_r. repeat (rel_pure_r _).
    iApply binary_fundamental; eauto with typeable.
  Qed.

  Definition or_inv x : iProp Σ :=
    (x ↦ᵢ #0 ∨ x ↦ᵢ #1)%I.
  Definition orN := nroot .@ "orN".
  Ltac close_shoot := iNext; (iLeft + iRight); by iFrame.

  Lemma assign_safe x :
    inv orN (or_inv x)
    ⊢ ▷ WP #x <- #1 {{ _, True }}.
  Proof.
    iIntros "#Hinv".
    iNext. iInv orN as ">[Hx | Hx]" "Hcl"; wp_store;
      (iMod ("Hcl" with "[-]"); first close_shoot); eauto.
  Qed.

  Lemma bin_log_or_commute Δ Γ (v1 v1' v2 v2' : val) :
    {Δ;Γ} ⊨ v1 ≤log≤ v1' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v2 ≤log≤ v2' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ or v2 v1 ≤log≤ or v1' v2' : TUnit.
  Proof.
    iIntros "Hv1 Hv2".
    unlock or. repeat rel_rec_r. repeat rel_rec_l.
    rel_alloc_l as x "Hx".
    rel_alloc_r as y "Hy".
    repeat rel_let_l. repeat rel_let_r.
    rel_fork_r as j "Hj". rel_seq_r.
    iMod (inv_alloc orN _ (or_inv x) with "[Hx]") as "#Hinv".
    { close_shoot. }
    rel_fork_l.
    iModIntro. iSplitR; [ by iApply assign_safe | ].
    rel_seq_l.
    rel_load_l_atomic.
    iInv orN as ">[Hx|Hx]" "Hcl";
      iExists _; iFrame; iModIntro; iNext; iIntros "Hx";
      rel_op_l; rel_if_l.
    + apply bin_log_related_spec_ctx.
      iDestruct 1 as (ρ1) "#Hρ1".
      (* TODO: tp tactics should be aware of that ^ *)
      tp_store j.
      rel_load_r.
      repeat (rel_pure_r _).
      iMod ("Hcl" with "[-Hv1 Hv2]"); first close_shoot.
      iApply (bin_log_related_app with "Hv2").
      iApply bin_log_related_unit.
    + rel_load_r.
      repeat (rel_pure_r _).
      iMod ("Hcl" with "[-Hv1 Hv2]"); first close_shoot.
      iApply (bin_log_related_app with "Hv1").
      iApply bin_log_related_unit.
  Qed.

  Lemma bin_log_or_idem_r Δ Γ (v v' : val) :
    {Δ;Γ} ⊨ v ≤log≤ v' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v #() ≤log≤ or v' v' : TUnit.
  Proof.
    iIntros  "Hlog".
    by iApply bin_log_or_choice_1_r_val.
  Qed.

  Lemma bin_log_or_idem_r_body Δ Γ e :
    Closed ∅ e →
    Γ ⊢ₜ e : TUnit →
    {Δ;Γ} ⊨ e ≤log≤ or (λ: <>, e) (λ: <>, e) : TUnit.
  Proof.
    iIntros (??).
    iPoseProof (bin_log_or_choice_1_r_body Δ _ e (λ: <>, e)) as "HZ"; eauto.
    unlock. eauto. (* TODO :( *)
  Qed.

  Lemma bin_log_or_idem_l Δ Γ (v v' : val) :
    {Δ;Γ} ⊨ v ≤log≤ v' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ or v v ≤log≤ v' #() : TUnit.
  Proof.
    iIntros  "Hlog".
    unlock or. repeat rel_rec_l.
    rel_alloc_l as x "Hx".
    repeat rel_let_l.
    iMod (inv_alloc orN _ (or_inv x)%I with "[Hx]") as "#Hinv".
    { close_shoot. }
    rel_fork_l.
    iModIntro. iSplitR; [ by iApply assign_safe | ].
    rel_seq_l.
    rel_load_l_atomic.
    iInv orN as ">[Hx|Hx]" "Hcl";
      iExists _; iFrame; iModIntro; iNext; iIntros "Hx";
        rel_op_l; rel_if_l.
    + iMod ("Hcl" with "[-Hlog]"); first close_shoot.
      iApply (bin_log_related_app with "Hlog").
      iApply bin_log_related_unit.
    + iMod ("Hcl" with "[-Hlog]"); first close_shoot.
      iApply (bin_log_related_app with "Hlog").
      iApply bin_log_related_unit.
  Qed.

  Lemma bin_log_or_bot_l Δ Γ (v v' : val) :
    {Δ;Γ} ⊨ v ≤log≤ v' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ or v bot ≤log≤ v' #() : TUnit.
  Proof.
    iIntros "Hlog".
    unlock or. repeat rel_rec_l.
    rel_alloc_l as x "Hx".
    repeat rel_let_l.
    iMod (inv_alloc orN _ (or_inv x)%I with "[Hx]") as "#Hinv".
    { close_shoot. }
    rel_fork_l.
    iModIntro. iSplitR; [ by iApply assign_safe | ].
    rel_seq_l.
    rel_load_l_atomic.
    iInv orN as ">[Hx|Hx]" "Hcl";
      iExists _; iFrame; iModIntro; iNext; iIntros "Hx";
      rel_op_l; rel_if_l.
    + iMod ("Hcl" with "[-Hlog]"); first close_shoot.
      iApply (bin_log_related_app with "Hlog").
      iApply bin_log_related_unit.
    + iMod ("Hcl" with "[-Hlog]"); first close_shoot.
      rel_apply_l bot_l.
  Qed.

  Lemma bin_log_or_bot_r Δ Γ (v v' : val) :
    {Δ;Γ} ⊨ v ≤log≤ v' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v #() ≤log≤ or v' bot : TUnit.
  Proof.
    iIntros "Hlog".
    iApply bin_log_or_choice_1_r_val; eauto.
  Qed.

  Lemma bin_log_or_assoc1 Δ Γ (v1 v1' v2 v2' v3 v3' : val) :
    {Δ;Γ} ⊨ v1 ≤log≤ v1' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v2 ≤log≤ v2' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ v3 ≤log≤ v3' : TArrow TUnit TUnit -∗
    {Δ;Γ} ⊨ or v1 (λ: <>, or v2 v3) ≤log≤ or (λ: <>, or v1' v2') v3' : TUnit.
  Proof.
    iIntros "Hv1 Hv2 Hv3".
    unlock or. simpl.
    rel_rec_l. rel_rec_l. (* TODO: SLOW, i guessbecause of solve_closed? *)
    rel_rec_r. rel_rec_r.
    rel_alloc_l as x "Hx".
    rel_alloc_r as y "Hy".
    repeat rel_let_l. repeat rel_let_r.
    rel_fork_r as j "Hj". rel_seq_r.
    iMod (inv_alloc orN _ (or_inv x) with "[Hx]") as "#Hinv".
    { close_shoot. }
    rel_fork_l.
    iModIntro. iSplitR; [ by iApply assign_safe | ].
    rel_seq_l.
    rel_load_l_atomic.
    iInv orN as ">[Hx|Hx]" "Hcl";
      iExists _; iFrame; iModIntro; iNext; iIntros "Hx";
      repeat (rel_pure_l _).
    - rel_load_r.
      repeat (rel_pure_r _).
      rel_alloc_r as y' "Hy'".
      rel_let_r. rel_fork_r as k "Hk".
      rel_seq_r. rel_load_r.
      repeat (rel_pure_r _).
      iMod ("Hcl" with "[-Hv1 Hv2 Hv3]") as "_"; first close_shoot.
      iApply (bin_log_related_app with "Hv1").
      iApply bin_log_related_unit.
    - iMod ("Hcl" with "[-Hv1 Hv2 Hv3 Hy Hj]") as "_"; first close_shoot.
      rel_alloc_l as x' "Hx'". rel_let_l.
      iClear "Hinv".
      iMod (inv_alloc orN _ (or_inv x') with "[Hx']") as "#Hinv".
      { close_shoot. }
      rel_fork_l.
      iModIntro. iSplitR; [ by iApply assign_safe | ].
      apply bin_log_related_spec_ctx.
      iDestruct 1 as (ρ1) "#Hρ1".
      rel_seq_l.
      rel_load_l_atomic.
      iInv orN as ">[Hx'|Hx']" "Hcl";
        iExists _; iFrame; iModIntro; iNext; iIntros "Hx'";
        repeat (rel_pure_l _).
      + rel_load_r; repeat (rel_pure_r _).
        rel_alloc_r as y' "Hy'".
        rel_let_r. rel_fork_r as k "Hk".
        rel_let_r.
        tp_store k.
        rel_load_r.
        repeat (rel_pure_r _).
        iMod ("Hcl" with "[-Hv1 Hv2 Hv3]") as "_"; first close_shoot.
        iApply (bin_log_related_app with "Hv2").
        iApply bin_log_related_unit.
      + tp_store j.
        rel_load_r; repeat (rel_pure_r _).
        iMod ("Hcl" with "[-Hv1 Hv2 Hv3]") as "_"; first close_shoot.
        iApply (bin_log_related_app with "Hv3").
        iApply bin_log_related_unit.
  Qed.

  Lemma bin_log_let1 Δ Γ (v : val) (e : expr) τ :
    Closed {["x"]} e →
    Γ ⊢ₜ subst "x" v e : τ →
    {Δ;Γ} ⊨ let: "x" := v in e ≤log≤ subst "x" v e : τ.
  Proof.
    iIntros (? Hτ).
    assert (Closed ∅ (Rec BAnon "x" e)).
    { unfold Closed. simpl. by rewrite right_id. }
    rel_let_l.
    by iApply binary_fundamental.
  Qed.

  Lemma bin_log_let2 Δ Γ (v : val) (e : expr) τ :
    Closed {["x"]} e →
    Γ ⊢ₜ subst "x" v e : τ →
    {Δ;Γ} ⊨ subst "x" v e ≤log≤ (let: "x" := v in e) : τ.
  Proof.
    iIntros (? Hτ).
    assert (Closed ∅ (Rec BAnon "x" e)).
    { unfold Closed. simpl. by rewrite right_id. }
    rel_let_r.
    by iApply binary_fundamental.
  Qed.

End contents.

Section theory.
  Lemma or_idempotency_1 (v : val) :
    ∅ ⊢ₜ v : TArrow TUnit TUnit →
    ∅ ⊨ or v v ≤ctx≤ v #() : TUnit.
  Proof.
    intros Ht.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_idem_l; eauto.
    by iApply binary_fundamental.
  Qed.
  Lemma or_idempotency_2 (v : val) :
    ∅ ⊢ₜ v : TArrow TUnit TUnit →
    ∅ ⊨ v #() ≤ctx≤ or v v : TUnit.
  Proof.
    intros Ht.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_idem_r; eauto.
    by iApply binary_fundamental.
  Qed.

  Lemma or_commutativity (v1 v2 : val) :
    ∅ ⊢ₜ v1 : TArrow TUnit TUnit →
    ∅ ⊢ₜ v2 : TArrow TUnit TUnit →
    ∅ ⊨ or v1 v2 ≤ctx≤ or v2 v1 : TUnit.
  Proof.
    intros Ht1 Ht2.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_commute; eauto; by iApply binary_fundamental.
  Qed.

  Lemma or_unital_1 (v : val) :
    ∅ ⊢ₜ v : TArrow TUnit TUnit →
    ∅ ⊨ or v bot ≤ctx≤ v #() : TUnit.
  Proof.
    intros Ht.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_bot_l; eauto.
    by iApply binary_fundamental.
  Qed.
  Lemma or_unital_2 (v : val) :
    ∅ ⊢ₜ v : TArrow TUnit TUnit →
    ∅ ⊨ v #() ≤ctx≤ or v bot : TUnit.
  Proof.
    intros Ht.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_bot_r; eauto.
    by iApply binary_fundamental.
  Qed.

  Lemma or_choice_1 (v1 v2 : val) :
    ∅ ⊢ₜ v1 : TArrow TUnit TUnit →
    ∅ ⊨ v1 #() ≤ctx≤ or v1 v2 : TUnit.
  Proof.
    intros Ht.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_choice_1_r_val; eauto.
    by iApply binary_fundamental.
  Qed.

  Lemma or_assoc_1 (v1 v2 v3 : val) :
    ∅ ⊢ₜ v1 : TArrow TUnit TUnit →
    ∅ ⊢ₜ v2 : TArrow TUnit TUnit →
    ∅ ⊢ₜ v3 : TArrow TUnit TUnit →
    ∅ ⊨ (or v1 (λ: <>, or v2 v3))%E ≤ctx≤ (or (λ: <>, or v1 v2) v3)%E : TUnit.
  Proof.
    intros Ht1 Ht2 Ht3.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    iApply bin_log_or_assoc1; eauto; by iApply binary_fundamental.
  Qed.

  (* The associativity in the other direction, assuming commutativity:
     (v1 or v2) or v3
   ≤ (v2 or v1) or v3   comm
   ≤ v3 or (v2 or v1)   comm
   ≤ (v3 or v2) or v1   assoc1
   ≤ (v2 or v3) or v1   comm
   ≤ v1 or (v2 or v3)
   *)
  (* Lemma or_assoc_2 (v1 v2 v3 : val) : *)
  (*   ∅ ⊢ₜ v1 : TArrow TUnit TUnit → *)
  (*   ∅ ⊢ₜ v2 : TArrow TUnit TUnit → *)
  (*   ∅ ⊢ₜ v3 : TArrow TUnit TUnit → *)
  (*   ∅ ⊨ (or (λ: <>, or v1 v2) v3)%E ≤ctx≤ (or v1 (λ: <>, or v2 v3))%E : TUnit. *)
    
End theory.

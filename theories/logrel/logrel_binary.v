From stdpp Require Import tactics.
From iris.proofmode Require Import tactics.
From iris.program_logic Require Export weakestpre.
From iris.base_logic Require Export big_op invariants.
From iris.algebra Require Import list.
From iris_logrel.logrel Require Export semtypes.
Import uPred.

Section bin_log_def.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).

  Definition interp_env (Γ : stringmap type) (E : coPset)
      (Δ : listC D) (vvs : stringmap (val * val)) : iProp Σ :=
    (⌜dom stringset Γ = dom stringset vvs⌝
    ∗ [∗ map] x ↦ τvv ∈ (map_zip Γ vvs), interp E (fst τvv) Δ (snd τvv))%I.

  Global Instance interp_env_persistent Γ E Δ vvs :
    Persistent (interp_env Γ E Δ vvs) := _.

  Global Instance interp_env_ne n Γ E :
    Proper (dist n ==> (=) ==> dist n) (interp_env Γ E).
  Proof. solve_proper. Qed.

  Definition bin_log_related_def (E : coPset)
      (Δ : list D) (Γ : stringmap type)
      (e e' : expr) (τ : type) : iProp Σ := (∀ (vvs : stringmap (val * val)) ρ,
    spec_ctx ρ -∗
    □ interp_env Γ ⊤ Δ vvs 
    -∗ interp_expr E (interp ⊤ τ) Δ
             (env_subst (fst <$> vvs) e, env_subst (snd <$> vvs) e'))%I.
  Definition bin_log_related_aux : seal bin_log_related_def. Proof. by eexists. Qed.
  Definition bin_log_related := unseal bin_log_related_aux.
  Definition bin_log_related_eq : bin_log_related = bin_log_related_def :=
    seal_eq bin_log_related_aux.

  Global Instance bin_log_related_ne E n :
    Proper (dist n ==> (=) ==> (=) ==> (=) ==> (=) ==> dist n) (bin_log_related E).
  Proof. rewrite bin_log_related_eq. solve_proper. Qed.
End bin_log_def.

Notation "⟦ Γ ⟧*" := (interp_env Γ).

Notation "'{' E ';' Δ ';' Γ '}' ⊨ e '≤log≤' e' : τ" :=
  (bin_log_related E Δ Γ e%E e'%E (τ)%F)
  (at level 74, E at level 50, Δ at next level, Γ at next level, e, e' at next level,
   τ at level 98,
   format "'[hv' '{' E ';'  Δ ';'  Γ '}'  ⊨  '/  ' e  '/' '≤log≤'  '/  ' e'  :  τ ']'").
Notation "'{' Δ ';' Γ '}' ⊨ e '≤log≤' e' : τ" :=
  (bin_log_related ⊤ Δ Γ e%E e'%E (τ)%F)
  (at level 74, Δ at level 50, Γ at next level, e, e' at next level,
   τ at level 98,
   format "'[hv' '{' Δ ';'  Γ '}'  ⊨  '/  ' e  '/' '≤log≤'  '/  ' e'  :  τ ']'").
Notation "Γ ⊨ e '≤log≤' e' : τ" :=
  (∀ Δ, bin_log_related ⊤ Δ Γ e%E e'%E (τ)%F)%I
  (at level 74, e, e' at next level,
   τ at level 98,
   format "'[hv' Γ  ⊨  '/  ' e  '/' '≤log≤'  '/  ' e'  :  τ ']'").
(* TODO: 
If I set the level for τ at 98 then the 
following wouldn't pass:

Lemma refinement1 `{logrelG Σ} Γ :
    Γ ⊨ #() ≤log≤ #() : (Unit → Unit) → TNat.

If the level is 99 then the following is not parsed.


   Lemma refinement1 `{logrelG Σ} Γ :
    Γ ⊨ #() ≤log≤ #() : (Unit → Unit) → TNat -∗ True.
*)

(** [interp_env] properties *)
Section interp_env_facts.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types τi : D.
  Implicit Types Δ : listC D.

  Lemma interp_env_dom Δ Γ E vvs :
    interp_env Γ E Δ vvs ⊢ ⌜dom stringset Γ = dom stringset vvs⌝.
  Proof. by iIntros "[% ?]". Qed.

  Lemma interp_env_Some_l Δ Γ E vvs x τ :
    Γ !! x = Some τ → interp_env Γ E Δ vvs ⊢ ∃ vv, ⌜vvs !! x = Some vv⌝ ∧ interp E τ Δ vv.
  Proof.
    iIntros (Hτ) "[Hdom HΓ]"; iDestruct "Hdom" as %Hdom.
    assert (x ∈ dom stringset vvs) as [v Hv]%elem_of_dom.
    { rewrite -Hdom. apply elem_of_dom. by eexists τ. }
    assert (map_zip Γ vvs !! x = Some (τ, v)) as Hτv.
    { rewrite map_lookup_zip_with.
      by rewrite Hτ /= Hv /=. }    
    iExists v; iSplit. done.    
    iApply (big_sepM_lookup _ _ _ _ Hτv with "HΓ").
  Qed.

  Lemma interp_env_nil Δ E : True ⊢ interp_env ∅ E Δ ∅.
  Proof.
    iIntros "_". iSplit.
    - iPureIntro. unfold_leibniz. by rewrite ?dom_empty.
    - rewrite map_zip_with_empty. auto.
  Qed.

  Lemma interp_env_cons (Δ : list D) (Γ : stringmap type)
     (vvs : stringmap (val * val)) E (τ : type) (vv : val * val) (x : string) :
    interp E τ Δ vv ∗ interp_env Γ E Δ vvs 
    ⊢ interp_env (<[x:=τ]> Γ) E Δ (<[x:=vv]> vvs).
  Proof.
    iIntros "[Hτ [Hdom HΓ]]". iDestruct "Hdom" as %Hdom. iSplit.
    - iPureIntro. by rewrite !dom_insert_L Hdom. (* TODO: RK: look at why this is so slow *)
    - rewrite -(map_insert_zip_with pair Γ vvs x (τ,vv)); auto.
      rewrite -insert_delete.
      rewrite big_sepM_insert; last by rewrite lookup_delete.
      iFrame "Hτ".
      iApply (big_sepM_mono _ _ (map_zip Γ vvs) with "HΓ").
      { apply delete_subseteq. }
      done.
  Qed.

  Lemma interp_env_ren Δ (Γ : stringmap type) E (vvs : stringmap (val * val)) (τi : D) :
    interp_env (⤉Γ) E (τi :: Δ) vvs
               ⊣⊢
    interp_env Γ E Δ vvs.
  Proof.
    apply sep_proper; [apply pure_proper|].
    - unfold_leibniz. by rewrite dom_fmap. 
    - rewrite map_zip_with_fmap_1. 
      rewrite map_zip_with_map_zip.
      generalize (map_zip Γ vvs).
      apply map_ind. 
      + rewrite fmap_empty. done.
      + intros i [σ ww] m Hm IH.
        rewrite fmap_insert.
        rewrite ?big_sepM_insert /=; auto; last first.
        { rewrite lookup_fmap. rewrite Hm. done. }
        rewrite IH. apply sep_proper; auto.
        apply (interp_weaken [] [τi] Δ).
  Qed.
End interp_env_facts.

(** Properties of the relational interpretation *)
Section related_facts.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types τi : D.
  Implicit Types Δ : listC D.

  (* We need this to be able to open and closed invariants in front of logrels *)
  Lemma fupd_logrel E1 E2 Δ Γ e e' τ :
    ((|={E1,E2}=> ({E2;Δ;Γ} ⊨ e ≤log≤ e' : τ))
     -∗ {E1;Δ;Γ} ⊨ e ≤log≤ e' : τ)%I.
  Proof.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros "H".
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    iMod "H" as "H".
    iSpecialize ("H" with "Hs [HΓ] Hj"); eauto.    
  Qed.

  Global Instance elim_fupd_logrel E1 E2 Δ Γ e e' P τ :
   ElimModal (|={E1,E2}=> P) P
     ({E1;Δ;Γ} ⊨ e ≤log≤ e' : τ) ({E2;Δ;Γ} ⊨ e ≤log≤ e' : τ).
  Proof.
    rewrite /ElimModal.
    iIntros "[HP HI]". iApply fupd_logrel.
    iMod "HP". iModIntro. by iApply "HI".
  Qed.

  Global Instance elim_bupd_logrel E Δ Γ e e' P τ :
   ElimModal (|==> P) P
     ({E;Δ;Γ} ⊨ e ≤log≤ e' : τ) ({E;Δ;Γ} ⊨ e ≤log≤ e' : τ).
  Proof.
    rewrite /ElimModal (bupd_fupd E).
    apply: elim_fupd_logrel.
  Qed.

  (* This + elim_modal_timless_bupd' is useful for stripping off laters of timeless propositions. *)
  Global Instance is_except_0_logrel E Δ Γ e e' τ :
    IsExcept0 ({E;Δ;Γ} ⊨ e ≤log≤ e' : τ).
  Proof.
    rewrite /IsExcept0.
    iIntros "HL".
    iApply fupd_logrel.
    by iMod "HL".
  Qed.

End related_facts.

(** Monadic-like operations on logrel *)
Section monadic.
  Context `{logrelG Σ}.

  Lemma related_ret E Δ Γ e1 e2 τ `{Closed ∅ e1} `{Closed ∅ e2} :
    interp_expr E ⟦ τ ⟧ Δ (e1,e2) -∗ {E;Δ;Γ} ⊨ e1 ≤log≤ e2 : τ%I.
  Proof.
    iIntros "Hτ".
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros (vvs ρ) "#Hs #HΓ".
    rewrite /env_subst !Closed_subst_p_id.
    iIntros (j K) "Hj /=".
    by iMod ("Hτ" with "Hj").
  Qed.

  Lemma related_bind_up E Δ R Γ (e1 e2 : expr) (τ τ' : type) (K K' : list ectx_item) :
    ({E;R::Δ;⤉Γ} ⊨ e1 ≤log≤ e2 : τ) -∗
    (∀ vv, ⟦ τ ⟧ (R::Δ) vv -∗ {⊤;Δ;Γ} ⊨ fill K (of_val (vv.1)) ≤log≤ fill K' (of_val (vv.2)) : τ') -∗
    ({E;Δ;Γ} ⊨ fill K e1 ≤log≤ fill K' e2 : τ').
  Proof.
    iIntros "Hm Hf".
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros (vvs ρ) "#Hs #HΓ".
    iIntros (j L) "Hj /=".
    iSpecialize ("Hm" with "Hs [HΓ]").
    { by rewrite interp_env_ren. }
    rewrite /env_subst !fill_subst -fill_app.
    iMod ("Hm" with "Hj") as "Hm /=".
    iModIntro.
    iApply wp_bind.
    iApply (wp_wand with "Hm").
    iIntros (v1). iDestruct 1 as (v2) "[Hj Hvv]".
    rewrite fill_app.
    replace (of_val v1) with (subst_p (fst <$> vvs) (of_val v1))
      by (apply Closed_subst_p_id; done).
    replace (of_val v2) with (subst_p (snd <$> vvs) (of_val v2))
      by (apply Closed_subst_p_id; done).
    rewrite -!fill_subst.
    iApply fupd_wp.
    iMod ("Hf" with "Hvv Hs HΓ Hj") as "Hf /=".
    by rewrite fill_subst Closed_subst_p_id.
  Qed.

End monadic.

Typeclasses Opaque interp_env.

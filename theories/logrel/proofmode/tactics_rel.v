From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import coq_tactics sel_patterns.
From iris.proofmode Require Export tactics.
From iris_logrel.logrel Require Export logrel_binary rules.
From iris_logrel.F_mu_ref_conc Require Export tactics pureexec.
Set Default Proof Using "Type".
Import lang.

(** General tactics *)
(** * Bind tactics for the relational interpretation *)
Lemma tac_rel_bind_l `{logrelG Σ} e' K ℶ E Δ Γ e t τ :
  e = fill K e' →
  envs_entails ℶ (bin_log_related E Δ Γ (fill K e') t τ) →
  envs_entails ℶ (bin_log_related E Δ Γ e t τ).
Proof. intros. subst. eauto. Qed.

Lemma tac_rel_bind_r `{logrelG Σ} t' K ℶ E Δ Γ e t τ :
  t = fill K t' →
  envs_entails ℶ (bin_log_related E Δ Γ e (fill K t') τ) →
  envs_entails ℶ (bin_log_related E Δ Γ e t τ).
Proof. intros. subst. eauto. Qed.

Tactic Notation "tac_bind_helper" :=
  lazymatch goal with
  | |- fill ?K ?e = fill _ ?efoc =>
     reshape_expr e ltac:(fun K' e' =>
       unify e' efoc;
       let K'' := eval cbn[app] in (K' ++ K) in
       replace (fill K e) with (fill K'' e') by (by rewrite ?fill_app))
  | |- ?e = fill _ ?efoc =>
     reshape_expr e ltac:(fun K' e' =>
       unify e' efoc;
       replace e with (fill K' e') by (by rewrite ?fill_app))
  end; reflexivity.

Tactic Notation "tac_bind_helper" open_constr(efoc) :=
  lazymatch goal with
  | |- fill ?K ?e = fill _ _ =>
     reshape_expr e ltac:(fun K' e' =>
       unify e' efoc;
       let K'' := eval cbn[app] in (K' ++ K) in
       replace (fill K e) with (fill K'' e') by (by rewrite ?fill_app))
  | |- ?e = fill _ _ =>
     reshape_expr e ltac:(fun K' e' =>
       unify e' efoc;
       replace e with (fill K' e') by (by rewrite ?fill_app))
  end; reflexivity.

Tactic Notation "rel_bind_l" open_constr(efoc) :=
  iStartProof;
  eapply (tac_rel_bind_l efoc);
  [ tac_bind_helper
  | (* new goal *)
  ].

Tactic Notation "rel_bind_r" open_constr(efoc) :=
  iStartProof;
  eapply (tac_rel_bind_r efoc);
  [tac_bind_helper
  | (* new goal *)
  ].

Ltac rel_reshape_cont_r tac :=
  lazymatch goal with
  | |- envs_entails _ (bin_log_related _ _ _ _ (fill ?K ?e) _) =>
    reshape_expr e ltac:(fun K' e' =>
      tac (K' ++ K) e')
  | |- envs_entails _ (bin_log_related _ _ _ _ ?e _) =>
    reshape_expr e ltac:(fun K' e' => tac K' e')
  end.

Ltac rel_reshape_cont_l tac :=
  lazymatch goal with
  | |- envs_entails _ (bin_log_related _ _ _ (fill ?K ?e) _ _) =>
    reshape_expr e ltac:(fun K' e' =>
      tac (K' ++ K) e')
  | |- envs_entails _ (bin_log_related _ _ _ ?e _ _) =>
    reshape_expr e ltac:(fun K' e' => tac K' e')
  end.

Ltac rel_bind_ctx_l K :=
  eapply (tac_rel_bind_l _ K);
  [reflexivity || tac_bind_helper
  |].

Ltac rel_bind_ctx_r K :=
  eapply (tac_rel_bind_r _ K);
  [reflexivity || tac_bind_helper
  |].

Tactic Notation "rel_vals" :=
  iStartProof;
  iApply bin_log_related_val; [ try solve_to_val | try solve_to_val | simpl ].

Tactic Notation "rel_finish" := by (rel_vals; simpl; eauto).

Tactic Notation "rel_apply_l" open_constr(lem) :=
  (iPoseProofCore lem as false true (fun H =>
    rel_reshape_cont_l ltac:(fun K e =>
      rel_bind_ctx_l K;
      iApplyHyp H;
      try iNext; simpl_subst/=)
    || lazymatch iTypeOf H with
      | Some (_,?P) => fail "rel_apply_l: Cannot apply" P
      end)); tac_rel_done.

Tactic Notation "rel_apply_r" open_constr(lem) :=
  (iPoseProofCore lem as false true (fun H =>
    rel_reshape_cont_r ltac:(fun K e =>
      rel_bind_ctx_r K;
      iApplyHyp H;
      try iNext; simpl_subst/=)
    || lazymatch iTypeOf H with
      | Some (_,?P) => fail "rel_apply_r: Cannot apply" P
      end)); tac_rel_done.

(** Pure reductions *)

Lemma tac_rel_pure_l `{logrelG Σ} K e1 ℶ E Δ Γ e e2 eres ϕ t τ (b : bool) :
  e = fill K e1 →
  PureExec ϕ e1 e2 →
  ϕ →
  ((b = true ∧ E = ⊤) ∨ b = false) →
  eres = fill K e2 →
  envs_entails ℶ (▷?b bin_log_related E Δ Γ eres t τ) →
  envs_entails ℶ (bin_log_related E Δ Γ e t τ).
Proof.
  intros Hfill Hpure Hϕ Hb ? Hp. subst.
  destruct b.
  - destruct Hb as [[_ ?] | Hb']; last by inversion Hb'. subst.
    by rewrite -(bin_log_pure_l Δ Γ K e1 e2 t τ _ _ Hϕ).
  - by rewrite -(bin_log_pure_masked_l Δ Γ E K e1 e2 t τ _ _ Hϕ).
Qed.

Tactic Notation "rel_pure_l" open_constr(ef) :=
  iStartProof;
  (simple eapply tac_rel_pure_l;
   [tac_bind_helper ef          (* e = fill K e1' *)
   |apply _                     (* PureExec ϕ e1 e2 *)
   |try (exact I || reflexivity || tac_rel_done)
   |first [left; split; reflexivity | right; reflexivity] (* E1 = E2? *)
   |simpl; reflexivity       (* eres = fill K e2 *)
   |try iNext; simpl_subst/= (* new goal *)])
  || fail "rel_pure_l: cannot find the reduct".

Tactic Notation "rel_rec_l" := rel_pure_l (App (Rec _ _ _) _) || rel_pure_l (App _ _).
Tactic Notation "rel_seq_l" := rel_rec_l.
Tactic Notation "rel_let_l" := rel_rec_l.
Tactic Notation "rel_fst_l" := rel_pure_l (Fst (Pair _ _)).
Tactic Notation "rel_snd_l" := rel_pure_l (Snd (Pair _ _)).
Tactic Notation "rel_proj_l" := rel_pure_l (_ (Pair _ _)).
Tactic Notation "rel_case_inl_l" := rel_pure_l (Case (InjL _) _ _).
Tactic Notation "rel_case_inr_l" := rel_pure_l (Case (InjR _) _ _).
Tactic Notation "rel_case_l" := rel_pure_l (Case _ _ _).
Tactic Notation "rel_binop_l" := rel_pure_l (BinOp _ _ _).
Tactic Notation "rel_op_l" := rel_binop_l.
Tactic Notation "rel_if_true_l" := rel_pure_l (If #true _ _).
Tactic Notation "rel_if_false_l" := rel_pure_l (If #false _ _).
Tactic Notation "rel_if_l" := rel_pure_l (If _ _ _).
Tactic Notation "rel_unfold_l" := rel_pure_l (Unfold (Fold _)).
Tactic Notation "rel_fold_l" := rel_unfold_l.
Tactic Notation "rel_tlam_l" := rel_pure_l (TApp (TLam _)).
Tactic Notation "rel_unpack_l" := rel_pure_l (Unpack (Pack _) _).
Tactic Notation "rel_pack_l" := rel_unpack_l.

Lemma tac_rel_pure_r `{logrelG Σ} K e1 ℶ E Δ Γ e e2 eres ϕ t τ :
  e = fill K e1 →
  PureExec ϕ e1 e2 →
  ϕ →
  nclose specN ⊆ E →
  eres = fill K e2 →
  envs_entails ℶ (bin_log_related E Δ Γ t (fill K e2) τ) →
  envs_entails ℶ (bin_log_related E Δ Γ t e τ).
Proof.
  intros Hfill Hpure Hϕ ?? Hp. subst.
  by rewrite -(bin_log_pure_r Δ Γ E K e1 e2 t τ).
Qed.

Tactic Notation "rel_pure_r" open_constr(ef) :=
  iStartProof;
  rel_reshape_cont_r ltac:(fun K e' =>
      unify e' ef;
      simple eapply (tac_rel_pure_r K e');
      [reflexivity                 (* e = fill K e1 *)
      |apply _                     (* PureExec ϕ e1 e2 *)
      |try (exact I || reflexivity || tac_rel_done)  (* φ *)
      |solve_ndisj  || fail "rel_pure_r: cannot solve ↑specN ⊆ ?"
      |simpl; reflexivity          (* eres = fill K e2 *)
      |simpl_subst/=               (* new goal *)])
  || fail "rel_pure_r: cannot find the reduct".

Tactic Notation "rel_rec_r" := rel_pure_r (App (Rec _ _ _) _) || rel_pure_r (App _ _).
Tactic Notation "rel_seq_r" := rel_rec_r.
Tactic Notation "rel_let_r" := rel_rec_r.
Tactic Notation "rel_fst_r" := rel_pure_r (Fst (Pair _ _)).
Tactic Notation "rel_snd_r" := rel_pure_r (Snd (Pair _ _)).
Tactic Notation "rel_proj_r" := rel_pure_r (_ (Pair _ _)).
Tactic Notation "rel_case_inl_r" := rel_pure_r (Case (InjL _) _ _).
Tactic Notation "rel_case_inr_r" := rel_pure_r (Case (InjL _) _ _).
Tactic Notation "rel_case_r" := rel_pure_r (Case _ _ _).
Tactic Notation "rel_binop_r" := rel_pure_r (BinOp _ _ _).
Tactic Notation "rel_op_r" := rel_binop_r.
Tactic Notation "rel_if_true_r" := rel_pure_r (If #true _ _).
Tactic Notation "rel_if_false_r" := rel_pure_r (If #false _ _).
Tactic Notation "rel_if_r" := rel_pure_r (If _ _ _).
Tactic Notation "rel_unfold_r" := rel_pure_r (Unfold (Fold _)).
Tactic Notation "rel_fold_r" := rel_unfold_r.
Tactic Notation "rel_tlam_r" := rel_pure_r (TApp (TLam _)).
Tactic Notation "rel_unpack_r" := rel_pure_r (Unpack (Pack _) _).
Tactic Notation "rel_pack_r" := rel_unpack_r.

(** Stateful reductions *)

(* Fork *)
Lemma tac_rel_fork_l `{logrelG Σ} ℶ Δ E e' K eres Γ e t τ :
  e = fill K (Fork e') →
  Closed ∅ e' →
  eres = fill K Unit →
  envs_entails ℶ (|={⊤,E}=> ▷ WP e' {{ _ , True%I }} ∗ bin_log_related E Δ Γ eres t τ) →
  envs_entails ℶ (bin_log_related ⊤ Δ Γ e t τ).
Proof.
  intros ????.
  subst e eres.
  rewrite -(bin_log_related_fork_l Δ Γ E); eassumption.
Qed.

Tactic Notation "rel_fork_l" :=
  iStartProof;
  eapply (tac_rel_fork_l);
    [tac_bind_helper || fail "rel_fork_l: cannot find 'fork'"
    |solve_closed       (* Closed ∅ e' *)
    |simpl; reflexivity (* eres = fill K () *)
    |simpl (* new goal *) ].

Lemma tac_rel_fork_r `{logrelG Σ} ℶ Δ E e' K Γ e t eres τ :
  nclose specN ⊆ E →
  e = fill K (Fork e') →
  Closed ∅ e' →
  eres = fill K Unit →
  envs_entails ℶ (∀ i, i ⤇ e' -∗ bin_log_related E Δ Γ t eres τ) →
  envs_entails ℶ (bin_log_related E Δ Γ t e τ).
Proof.
  intros ?????.
  subst e eres.
  rewrite -(bin_log_related_fork_r Δ Γ E); eassumption.
Qed.

Tactic Notation "rel_fork_r" "as" ident(i) constr(H) :=
  iStartProof;
  eapply (tac_rel_fork_r);
    [solve_ndisj || fail "rel_fork_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper || fail "rel_fork_r: cannot find 'fork'"
    |solve_closed         (* Closed ∅ e' *)
    |simpl; reflexivity   (* eres = fill K () *)
    |simpl; iIntros (i) H (* new goal *)].

(* Alloc *)
Tactic Notation "rel_alloc_l_atomic" := rel_apply_l bin_log_related_alloc_l.

Lemma tac_rel_alloc_l_simp `{logrelG Σ} ℶ1 ℶ2 Δ Γ e t e' v' K τ :
  e = fill K (Alloc e') →
  IntoLaterNEnvs 1 ℶ1 ℶ2 →
  to_val e' = Some v' →
  (envs_entails ℶ2 (∀ l,
     (l ↦ᵢ v' -∗ bin_log_related ⊤ Δ Γ (fill K (Lit (Loc l))) t τ))) →
  envs_entails ℶ1 (bin_log_related ⊤ Δ Γ e t τ).
Proof.
  intros ???; subst. rewrite /envs_entails.
  rewrite into_laterN_env_sound /=.
  rewrite -(bin_log_related_alloc_l' Δ Γ); eauto.
  apply uPred.later_mono.
Qed.

Tactic Notation "rel_alloc_l" "as" ident(l) constr(H) :=
  iStartProof;
  eapply tac_rel_alloc_l_simp;
    [tac_bind_helper       (* e = fill K' .. *)
    |apply _               (* IntoLaterNEnvs _ _ _ *)
    |solve_to_val          (* to_val e' = Some v *)
    |simpl; iIntros (l) H  (* new goal *)].

Lemma tac_rel_alloc_r `{logrelG Σ} ℶ E Δ Γ t' v' K' e t τ :
  nclose specN ⊆ E →
  t = fill K' (Alloc t') →
  to_val t' = Some v' →
  envs_entails ℶ (∀ l, l ↦ₛ v' -∗ bin_log_related E Δ Γ e (fill K' (Lit (Loc l))) τ) →
  envs_entails ℶ (bin_log_related E Δ Γ e t τ).
Proof.
  intros ????.
  subst t.
  rewrite -(bin_log_related_alloc_r Δ Γ E); eassumption.
Qed.

Tactic Notation "rel_alloc_r" "as" ident(l) constr(H) :=
  iStartProof;
  eapply (tac_rel_alloc_r);
    [solve_ndisj     || fail "rel_alloc_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper || fail "rel_alloc_r: cannot find 'alloc'"
    |solve_to_val    || fail "rel_alloc_r: argument not a value"
    |simpl; iIntros (l) H (* new goal *)].

Tactic Notation "rel_alloc_r" :=
  let l := fresh in
  let H := iFresh "H" in
  rel_alloc_r as l H.

(* Load *)
Tactic Notation "rel_load_l_atomic" := rel_apply_l bin_log_related_load_l.

Lemma tac_rel_load_l_simp `{logrelG Σ} ℶ1 ℶ2 i1 Δ Γ (l : loc) q v K e t eres τ :
  e = fill K (Load (# l)) →
  IntoLaterNEnvs 1 ℶ1 ℶ2 →
  envs_lookup i1 ℶ2 = Some (false, l ↦ᵢ{q} v)%I →
  eres = fill K (of_val v) →
  envs_entails ℶ2 (bin_log_related ⊤ Δ Γ eres t τ) →
  envs_entails ℶ1 (bin_log_related ⊤ Δ Γ e t τ).
Proof.
  intros ???? HΔ2; subst. rewrite /envs_entails.
  rewrite into_laterN_env_sound envs_lookup_split //; simpl.
  rewrite uPred.later_sep.
  rewrite HΔ2.
  apply uPred.wand_elim_l'.
  by rewrite -(bin_log_related_load_l' Δ Γ).
Qed.

Tactic Notation "rel_load_l" :=
  iStartProof;
  eapply (tac_rel_load_l_simp);
    [tac_bind_helper (* e = fill K' .. *)
    |apply _  (* IntoLaterNenvs _ Δ1 Δ2 *)
    |iAssumptionCore || fail 3 "rel_load_l: cannot find ? ↦ᵢ{?} ?"
    |simpl; reflexivity  (* eres = fill K (of_val v) *)
    |simpl (* new goal *)].

Lemma tac_rel_load_r `{logrelG Σ} ℶ1 ℶ2 E Δ Γ K i1 (l : loc) q e t tres τ v :
  nclose specN ⊆ E →
  t = fill K (Load (# l)) →
  envs_lookup i1 ℶ1 = Some (false, l ↦ₛ{q} v)%I →
  envs_simple_replace i1 false
    (Esnoc Enil i1 (l ↦ₛ{q} v)%I) ℶ1 = Some ℶ2 →
  tres = fill K (of_val v) →
  envs_entails ℶ2 (bin_log_related E Δ Γ e tres τ) →
  envs_entails ℶ1 (bin_log_related E Δ Γ e t τ).
Proof.
  intros ????? Hg. rewrite /envs_entails.
  rewrite (envs_simple_replace_sound ℶ1 ℶ2 i1) //; simpl.
  rewrite right_id.
  subst t tres.
  rewrite {1}(bin_log_related_load_r Δ Γ E); [ | eassumption ].
  rewrite Hg.
  apply uPred.wand_elim_l.
Qed.

Tactic Notation "rel_load_r" :=
  iStartProof;
  eapply (tac_rel_load_r);
    [solve_ndisj || fail "rel_load_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper (* e = fill K (Store (# l) e') *)
    |iAssumptionCore || fail "rel_load_r: cannot find ? ↦ₛ{?} ?"
    |env_cbv; reflexivity || fail "rel_load_r: this should not happen"
    |simpl; reflexivity  (* tres = fill K (of_val v) *)
    |simpl (* new goal *)].

(* Store *)
Tactic Notation "rel_store_l_atomic" := rel_apply_l bin_log_related_store_l.

Lemma tac_rel_store_l_simp `{logrelG Σ} ℶ1 ℶ2 ℶ3 i1 Δ Γ K (l : loc) v e' v' e t eres τ :
  e = fill K (Store (# l) e') →
  to_val e' = Some v' →
  IntoLaterNEnvs 1 ℶ1 ℶ2 →
  envs_lookup i1 ℶ2 = Some (false, l ↦ᵢ v)%I →
  envs_simple_replace i1 false (Esnoc Enil i1 (l ↦ᵢ v')) ℶ2 = Some ℶ3 →
  eres = fill K Unit →
  envs_entails ℶ3 (bin_log_related ⊤ Δ Γ eres t τ) →
  envs_entails ℶ1 (bin_log_related ⊤ Δ Γ e t τ).
Proof.
  intros ?????? Hg. rewrite /envs_entails.
  subst e eres.
  rewrite into_laterN_env_sound envs_simple_replace_sound //; simpl.
  rewrite uPred.later_sep.
  rewrite right_id.
  rewrite (bin_log_related_store_l' Δ Γ). 2: eassumption.
  rewrite Hg.
  apply uPred.wand_elim_l.
Qed.

Tactic Notation "rel_store_l" :=
  iStartProof;
  eapply (tac_rel_store_l_simp);
    [tac_bind_helper (* e = fill K' .. *)    
    |solve_to_val (* to_val e' = Some v' *)
    |apply _      (* IntoLaterNEnvs *)
    |iAssumptionCore || fail "rel_store_l: cannot find '? ↦ᵢ ?'"
    |env_cbv; reflexivity || fail "rel_store_l: this should not happen"
    |simpl; reflexivity  (* eres = fill K () *)
    |simpl (* new goal *)].

Lemma tac_rel_store_r `{logrelG Σ} ℶ1 ℶ2 E Δ Γ K i1 (l : loc) v' e e' t tres τ v :
  nclose specN ⊆ E →
  t = fill K (Store (# l) e') →
  to_val e' = Some v' →
  envs_lookup i1 ℶ1 = Some (false, l ↦ₛ v)%I →
  envs_simple_replace i1 false (Esnoc Enil i1 (l ↦ₛ v')) ℶ1 = Some ℶ2 →
  tres = fill K Unit →
  envs_entails ℶ2 (bin_log_related E Δ Γ e tres τ) →
  envs_entails ℶ1 (bin_log_related E Δ Γ e t τ).
Proof.
  intros ?????? Hg. rewrite /envs_entails.
  rewrite envs_simple_replace_sound //; simpl.
  rewrite right_id.
  subst t tres.
  rewrite (bin_log_related_store_r Δ Γ E); [ | eassumption..].
  rewrite Hg.
  apply uPred.wand_elim_l.
Qed.

Tactic Notation "rel_store_r" :=
  iStartProof;
  eapply (tac_rel_store_r);
    [solve_ndisj || fail "rel_store_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper (* e = fill K' (Store (# l) e') *)
    |solve_to_val (* to_val e' = Some v *)
    |iAssumptionCore || fail "rel_store_l: cannot find ? ↦ₛ ?"
    |env_cbv; reflexivity || fail "rel_store_r: this should not happen"
    |simpl; reflexivity  (* tres = fill K () *)
    |simpl (* new goal *)].

(* CAS *)
Tactic Notation "rel_cas_l_atomic" := rel_apply_l bin_log_related_cas_l.

Lemma tac_rel_cas_fail_r `{logrelG Σ} ℶ1 ℶ2 E Δ Γ K i1 (l : loc) e t e1 e2 v1 v2 tres τ v :
  nclose specN ⊆ E →
  t = fill K (CAS #l e1 e2) →
  to_val e1 = Some v1 →
  to_val e2 = Some v2 →
  envs_lookup i1 ℶ1 = Some (false, l ↦ₛ v)%I →
  v ≠ v1 →
  envs_simple_replace i1 false 
    (Esnoc Enil i1 (l ↦ₛ v)%I) ℶ1 = Some ℶ2 →
  tres = fill K #false →
  envs_entails ℶ2 (bin_log_related E Δ Γ e tres τ) →
  envs_entails ℶ1 (bin_log_related E Δ Γ e t τ).
Proof.
  intros ???????? Hg. rewrite /envs_entails.
  rewrite (envs_simple_replace_sound ℶ1 ℶ2 i1) //; simpl. 
  rewrite right_id.
  subst t tres.
  rewrite {1}(bin_log_related_cas_fail_r Δ Γ E _ l e1 e2 v1 v2 v); eauto.
  rewrite Hg.
  apply uPred.wand_elim_l.
Qed.

Tactic Notation "rel_cas_fail_r" :=
  iStartProof;
  eapply (tac_rel_cas_fail_r);
    [solve_ndisj || fail "rel_cas_fail_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper || fail "rel_cas_fail_r: cannot find 'CAS ..'"
    |solve_to_val
    |solve_to_val
    |iAssumptionCore || fail "rel_cas_fail_l: cannot find ? ↦ₛ ?" 
    |try fast_done (* v ≠ v1 *)
    |env_cbv; reflexivity || fail "rel_load_r: this should not happen"
    |simpl; reflexivity  (* tres = fill K false *)
    |simpl (* new goal *)].


Lemma tac_rel_cas_suc_r `{logrelG Σ} ℶ1 ℶ2 E Δ Γ K i1 (l : loc) e t e1 e2 v1 v2 tres τ v :
  nclose specN ⊆ E →
  t = fill K (CAS #l e1 e2) →
  to_val e1 = Some v1 →
  to_val e2 = Some v2 →
  envs_lookup i1 ℶ1 = Some (false, l ↦ₛ v)%I →
  v = v1 →
  envs_simple_replace i1 false 
    (Esnoc Enil i1 (l ↦ₛ v2)%I) ℶ1 = Some ℶ2 →
  tres = fill K # true →
  envs_entails ℶ2 (bin_log_related E Δ Γ e tres τ) →
  envs_entails ℶ1 (bin_log_related E Δ Γ e t τ).
Proof.
  intros ???????? Hg. rewrite /envs_entails.
  rewrite (envs_simple_replace_sound ℶ1 ℶ2 i1) //; simpl.
  rewrite right_id.
  subst t tres.
  rewrite {1}(bin_log_related_cas_suc_r Δ Γ E _ l e1 e2 v1 v2 v); eauto.
  rewrite Hg.
  apply uPred.wand_elim_l.
Qed.

Tactic Notation "rel_cas_suc_r" :=
  iStartProof;
  eapply (tac_rel_cas_suc_r);
    [solve_ndisj || fail "rel_cas_suc_r: cannot prove 'nclose specN ⊆ ?'"
    |tac_bind_helper || fail "rel_cas_suc_r: cannot find 'CAS ..'"
    |solve_to_val
    |solve_to_val
    |iAssumptionCore || fail "rel_cas_suc_l: cannot find ? ↦ₛ ?" 
    |try fast_done (* v = v1 *)
    |env_cbv; reflexivity || fail "rel_load_r: this should not happen"
    |simpl; reflexivity  (* tres = fill K true *)
    |simpl (* new goal *)].

(* TODO: move this to the tests folder *)
Section test.
  From iris_logrel.F_mu_ref_conc Require Import reflection.
  Context `{logrelG Σ}.
  
  Definition choiceN : namespace := nroot .@ "choice".

  Definition choice_inv y y' : iProp Σ :=
    (∃ f : bool, y ↦ᵢ # f ∗ y' ↦ₛ # f)%I.

  Definition storeFalse : val := λ: "y", "y" <- # false.

  Lemma test_store Γ Δ y y':
    inv choiceN (choice_inv y y')
    -∗ {Δ;Γ} ⊨ storeFalse (Fst (#y, #3)) ≤log≤ (storeFalse #y') : TUnit.
  Proof.
    iIntros "#Hinv".
    unfold storeFalse. unlock.
    rel_fst_l.
    rel_rec_l.
    rel_rec_r.

    rel_apply_l bin_log_related_store_l.
    iInv choiceN as (f) ">[Hy Hy']" "Hcl".
    iExists _. iFrame "Hy".
    iModIntro. iNext. iIntros "Hy".
    rel_store_r. simpl.

    iMod ("Hcl" with "[Hy Hy']").
    { iNext. iExists _. iFrame. }


    rel_vals; eauto.
  Qed.

End test.

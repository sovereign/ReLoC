From iris.proofmode Require Import tactics.
From iris.program_logic Require Import ectx_lifting.
From iris_logrel.logrel Require Export logrel_binary.
From iris_logrel.logrel Require Import rules_threadpool tactics_threadpool.
From iris_logrel.F_mu_ref_conc Require Import tactics pureexec.

(** * Properties of the relational interpretation *)
Section properties.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types e : expr.
  Implicit Types Δ : listC D.
  Hint Resolve to_of_val.

  (** * Primitive rules *)

  (** [fupd_logrel] is defined in [logrel_binary.v] *)

  Lemma bin_log_related_arrow_val Δ Γ (f x f' x' : binder) (e e' eb eb' : expr) (τ τ' : type) :
    e = (rec: f x := eb)%E →
    e' = (rec: f' x' := eb')%E →
    Closed ∅ e →
    Closed ∅ e' →
    □(∀ v1 v2, ⟦ τ ⟧ Δ (v1, v2) -∗
      {Δ;Γ} ⊨ App e (of_val v1) ≤log≤ App e' (of_val v2) : τ') -∗
    {Δ;Γ} ⊨ e ≤log≤ e' : TArrow τ τ'.
  Proof.
    iIntros (????) "#H".
    subst e e'.
    rewrite bin_log_related_eq.
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj".
    cbn-[subst_p].
    rewrite {2}/env_subst Closed_subst_p_id.
    iModIntro. iApply wp_value.
    { rewrite /IntoVal. simpl. erewrite decide_left. done. }
    rewrite /env_subst Closed_subst_p_id.
    iExists (RecV f' x' eb').
    iFrame "Hj". iAlways. iIntros ([v1 v2]) "Hvv".
    iSpecialize ("H" $! v1 v2 with "Hvv Hs []").
    { iAlways. iApply "HΓ". }
    assert (Closed ∅ ((rec: f x := eb) v1)).
    { unfold Closed in *. simpl.
      intros. split_and?; auto. apply of_val_closed. }
    assert (Closed ∅ ((rec: f' x' := eb') v2)).
    { unfold Closed in *. simpl.
      intros. split_and?; auto. apply of_val_closed. }
    rewrite /env_subst. rewrite !Closed_subst_p_id. done.
  Qed.

  Notation "P ∗-∗ Q" := ((P -∗ Q) ∗ (Q -∗ P))%I (at level 50).
  Lemma interp_val_arrow τ σ Δ (v v' : val) ρ :
    spec_ctx ρ -∗
    ⟦ τ → σ ⟧ Δ (v, v')
              ∗-∗
    (□ (∀ (w w' : val), ⟦ τ ⟧ Δ (w, w')
      -∗ {Δ;∅} ⊨ v w ≤log≤ v' w' : σ))%I.
  Proof.
    iIntros "#Hspec".
    iSplitL.
    - iIntros "/= #Hvv !#".
      iIntros (w w') "#Hw".
      iApply related_ret.
      iApply ("Hvv" $! (w, w') with "Hw").
    - iIntros "#Hvv /= !#".
      iIntros ([w w']) "#Hww /=".
      iSpecialize ("Hvv" with "Hww").
      rewrite bin_log_related_eq /bin_log_related_def.
      iIntros (j K) "Hj /=".
      iSpecialize ("Hvv" $! ∅ ρ with "Hspec []").
      { iAlways. by iApply interp_env_nil. }
      rewrite /interp_expr /=.
      iSpecialize ("Hvv" $! j K).
      rewrite /env_subst !fmap_empty !subst_p_empty.
      by iMod ("Hvv" with "Hj").
  Qed.

  Lemma bin_log_related_weaken_2 τi E Δ Γ e1 e2 (τ : type) :
    {E;Δ;Γ} ⊨ e1 ≤log≤ e2 : τ -∗
    {E;τi::Δ;⤉Γ} ⊨ e1 ≤log≤ e2 : τ.[ren (+1)].
  Proof.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros "Hlog" (vvs ρ) "#Hs #HΓ".
    iSpecialize ("Hlog" $! vvs with "Hs [HΓ]").
    { by rewrite interp_env_ren. }
    unfold interp_expr.
    iIntros (j K) "Hj /=".
    iMod ("Hlog" with "Hj") as "Hlog".
    iApply (wp_mono with "Hlog").
    iIntros (v). simpl.
    iDestruct 1 as (v') "[Hj Hτ]".
    iExists v'. iFrame.
    by rewrite -interp_ren.
  Qed.

  Lemma bin_log_related_spec_ctx Δ Γ E e e' τ ℶ :
    envs_entails ℶ ((∃ ρ, spec_ctx ρ) -∗ {E;Δ;Γ} ⊨ e ≤log≤ e' : τ)%I →
    envs_entails ℶ ({E;Δ;Γ} ⊨ e ≤log≤ e' : τ)%I.
  Proof.
    rewrite /envs_entails => Hp.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros "Hctx". iIntros (vvs ρ') "#Hspec".
    rewrite (persistent (spec_ctx _)).
    rewrite (uPred.persistently_sep_dup (spec_ctx _)).
    iDestruct "Hspec" as "#[Hspec #Hspec']".
    iRevert "Hspec'".
    rewrite (uPred.persistently_elim (spec_ctx _)).
    iAssert (∃ ρ, spec_ctx ρ)%I as "Hρ".
    { eauto. }
    iClear "Hspec".
    iRevert (vvs ρ').
    fold (bin_log_related_def E Δ Γ e e' τ).
    rewrite -bin_log_related_eq.
    iApply (Hp with "Hctx Hρ").
  Qed.

  (** ** Monadic rules *)
  Lemma related_bind E Δ Γ (e1 e2 : expr) (τ τ' : type) (K K' : list ectx_item) :
    ({E;Δ;Γ} ⊨ e1 ≤log≤ e2 : τ) -∗
    (∀ vv, ⟦ τ ⟧ Δ vv -∗ {Δ;Γ} ⊨ fill K (of_val (vv.1)) ≤log≤ fill K' (of_val (vv.2)) : τ') -∗
    ({E;Δ;Γ} ⊨ fill K e1 ≤log≤ fill K' e2 : τ').
  Proof.
    iIntros "Hm Hf".
    iApply (related_bind_up _ _ (λne _, True%I) _ _ _ (τ.[ren (+1)]) with "[Hm] [Hf]").
    - iApply (bin_log_related_weaken_2 with "Hm").
    - iIntros (vv) "Hvv /=".
      rewrite -interp_ren.
      by iApply "Hf".
  Qed.

  Lemma bin_log_related_val Δ Γ E e e' τ v v' :
    to_val e = Some v →
    to_val e' = Some v' →
    (|={E,⊤}=> ⟦ τ ⟧ Δ (v, v')) ⊢ {E;Δ;Γ} ⊨ e ≤log≤ e' : τ.
  Proof.
    iIntros (He He') "Hτ".
    rewrite interp_ret; eauto.
    by rewrite -related_ret.
  Qed.

  (** ** Forward reductions on the LHS *)

  (* TODO: should this be an instance? *)
  Lemma pureexec_subst_p ϕ e e' es :
    PureExec ϕ e e' →
    PureExec ϕ (subst_p es e) (subst_p es e').
  Proof.
    intros Hpure.
    apply hoist_pred_pure_exec. intros Hϕ.
    assert (Hsafe : ∀ σ, reducible (subst_p es e) σ).
    { intros; apply subst_p_safe; eauto using pure_exec_safe. }
    assert (to_val (subst_p es e) = None) as Hval.
    { destruct (Hsafe ∅) as [e2 [σ2 [efs Hs]]].
      by eapply language.val_stuck. }
    split; eauto. intros.
    cut (σ1 = σ2 ∧ subst_p es e' = e2' ∧ [] = efs); first by naive_solver.
    eapply subst_p_det; eauto.
    - intros σ. destruct (pure_exec_safe σ Hϕ) as (? & ? & ? & Hsafez).
      destruct (pure_exec_puredet _ _ _ _ Hϕ Hsafez) as (?&?&?).
      simplify_eq. apply Hsafez.
    - intros ???? Hs.
      destruct (pure_exec_puredet _ _ _ _ Hϕ Hs) as (?&?&?).
      by simplify_eq.
  Qed.

  Lemma bin_log_pure_l Δ (Γ : stringmap type)
    (K' : list ectx_item) (e e' t : expr) (τ : type) ϕ :
    PureExec ϕ e e' →
    ϕ →
    ▷ ({Δ;Γ} ⊨ fill K' e' ≤log≤ t : τ)
    ⊢ {Δ;Γ} ⊨ fill K' e ≤log≤ t : τ.
  Proof.
    intros Hpure Hϕ.
    rewrite bin_log_related_eq.
    iIntros "IH" (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst.
    iApply wp_pure_step_later; [ | apply Hϕ | ].
    { apply pure_exec_ctx; first apply _.
      apply pureexec_subst_p. eassumption. }
    iModIntro. iNext. iApply fupd_wp.
    iSpecialize ("IH" with "Hs [HΓ] Hj"); first done.
    by rewrite /env_subst fill_subst.
  Qed.

  Lemma bin_log_pure_masked_l (Δ : list D) (Γ : stringmap type) (E : coPset)
    (K' : list ectx_item) (e e' t : expr) (τ : type) ϕ :
    PureExec ϕ e e' →
    ϕ →
    {E;Δ;Γ} ⊨ fill K' e' ≤log≤ t : τ
    ⊢ {E;Δ;Γ} ⊨ fill K' e ≤log≤ t : τ.
  Proof.
    intros Hpure Hϕ.
    rewrite bin_log_related_eq.
    iIntros "IH".
    iIntros (vvs ρ) "#Hs #HΓ"; iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst.
    iApply wp_pure_step_later; [ | apply Hϕ | ].
    { apply pure_exec_ctx; first apply _.
      apply pureexec_subst_p. eassumption. }
    iMod ("IH" with "Hs [HΓ] Hj") as "IH"; auto.
    iModIntro. iNext. simpl.
    rewrite /env_subst fill_subst /= //.
  Qed.

  Lemma bin_log_related_wp_l Δ Γ K e1 e2 τ
   (Hclosed1 : Closed ∅ e1) :
   (WP e1 {{ v,
     {Δ;Γ} ⊨ fill K (of_val v) ≤log≤ e2 : τ }})%I -∗
   {Δ;Γ} ⊨ fill K e1 ≤log≤ e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "He".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K') "Hj /=".
    rewrite /env_subst fill_subst /=.
    rewrite Closed_subst_p_id.
    iApply (wp_bind (subst_ctx _ K)).
    iApply (wp_wand with "He").
    iModIntro. iIntros (v) "Hv".
    iMod ("Hv" with "Hs [HΓ] Hj"); auto.
    rewrite /env_subst fill_subst /=.
    by rewrite of_val_subst_p.
  Qed.

  Lemma bin_log_related_wp_atomic_l Δ Γ (E : coPset) K e1 e2 τ
   (Hatomic : Atomic WeaklyAtomic e1)
   (Hclosed1 : Closed ∅ e1) :
   (|={⊤,E}=> WP e1 @ E {{ v,
     {E;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ e2 : τ }})%I -∗
   {Δ;Γ} ⊨ fill K e1 ≤log≤ e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "Hlog".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K') "Hj /=". iModIntro.
    rewrite /env_subst fill_subst /=.
    rewrite Closed_subst_p_id.
    iApply (wp_bind (subst_ctx _ K)).
    iApply wp_atomic; auto.
    iMod "Hlog" as "He". iModIntro.
    iApply (wp_wand with "He").
    iIntros (v) "Hlog".
    iSpecialize ("Hlog" with "Hs [HΓ]"); first by iFrame.
    iSpecialize ("Hlog" with "Hj"). simpl.
    rewrite /env_subst fill_subst /=. rewrite of_val_subst_p.
    by iMod "Hlog".
  Qed.

  (** ** Forward reductions on the RHS *)

  Lemma bin_log_pure_r Δ Γ E K' e e' t τ
    (Hspec : nclose specN ⊆ E) ϕ :
    PureExec ϕ e e' →
    ϕ →
    {E;Δ;Γ} ⊨ t ≤log≤ fill K' e' : τ
    ⊢ {E;Δ;Γ} ⊨ t ≤log≤ fill K' e : τ.
  Proof.
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros (Hpure Hϕ) "Hlog".
    iIntros (vvs ρ) "#Hs #HΓ".
    iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst -fill_app.
    assert (PureExec ϕ (subst_p (snd <$> vvs) e) (subst_p (snd <$> vvs) e')).
    { apply pureexec_subst_p. eassumption. }
    tp_pure j; auto.
    rewrite fill_app -fill_subst.
    iDestruct ("Hlog" with "Hs [HΓ] Hj") as "Hlog"; auto.
  Qed.

  Lemma bin_log_related_step_r Φ Δ Γ E K' e1 e2 τ
    (Hclosed2 : Closed ∅ e2) :
    (∀ ρ j K, spec_ctx ρ -∗ (j ⤇ fill K e2 ={E}=∗ ∃ v, j ⤇ fill K (of_val v)
                  ∗ Φ v)) -∗
    (∀ v, Φ v -∗ {E;Δ;Γ} ⊨ e1 ≤log≤ fill K' (of_val v) : τ) -∗
    {E;Δ;Γ} ⊨ e1 ≤log≤ fill K' e2 : τ.
  Proof.
    rewrite bin_log_related_eq.
    iIntros "He Hlog".
    iIntros (vvs ρ) "#Hs #HΓ". iIntros (j K) "Hj /=".
    rewrite /env_subst fill_subst /= -fill_app.
    rewrite !Closed_subst_p_id.
    iMod ("He" $! ρ j with "Hs Hj") as (v) "[Hj Hv]".
    iSpecialize ("Hlog" $! v with "Hv Hs [HΓ]"); first by iFrame.
    rewrite /env_subst fill_app fill_subst.
    rewrite of_val_subst_p /=.
    iSpecialize ("Hlog" with "Hj"); done.
  Qed.

  Lemma bin_log_related_alloc_r Δ Γ E K e v t τ
    (Hmasked : nclose specN ⊆ E)
    (Heval : to_val e = Some v) :
    (∀ l : loc, l ↦ₛ v -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K #l : τ)%I
    -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K (Alloc e) : τ.
  Proof.
    iIntros "Hlog".
    pose (Φ := (fun w => ∃ l : loc, ⌜w = (# l)⌝ ∗ l ↦ₛ v)%I).
    iApply (bin_log_related_step_r Φ with "[]").
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=".
      tp_alloc j as l "Hl". iExists (# l).
      iFrame. iExists l. eauto. }
    iIntros (v') "He'". iDestruct "He'" as (l) "[% Hl]". subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_load_r Δ Γ E K l q v t τ
    (Hmasked : nclose specN ⊆ E) :
    l ↦ₛ{q} v -∗
    (l ↦ₛ{q} v -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K (of_val v) : τ)
    -∗ {E;Δ;Γ} ⊨ t ≤log≤ (fill K !#l) : τ.
  Proof.
    iIntros "Hl Hlog".
    pose (Φ := (fun w => ⌜w = v⌝ ∗ l ↦ₛ{q} v)%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=". iExists v.
      tp_load j.
      iFrame. eauto. }
    iIntros (?) "[% Hl]"; subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_store_r Δ Γ E K l e e' v v' τ
    (Hmasked : nclose specN ⊆ E) :
    to_val e' = Some v' →
    l ↦ₛ v -∗
    (l ↦ₛ v' -∗ {E;Δ;Γ} ⊨ e ≤log≤ fill K (#()) : τ) -∗
    {E;Δ;Γ} ⊨ e ≤log≤ fill K (#l <- e') : τ.
  Proof.
    iIntros (?) "Hl Hlog".
    pose (Φ := (fun w => ⌜w = #()⌝ ∗ l ↦ₛ v')%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=". iExists #().
      tp_store j.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_cas_fail_r Δ Γ E K l e1 e2 v1 v2 v t τ :
    nclose specN ⊆ E →
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v ≠ v1 →
    l ↦ₛ v -∗
    (l ↦ₛ v -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K #false : τ)
    -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K (CAS #l e1 e2) : τ.
  Proof.
    iIntros (????) "Hl Hlog".
    pose (Φ := (fun (w : val) => ⌜w = #false⌝ ∗ l ↦ₛ v)%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=".
      tp_cas_fail j; auto.
      iExists #false. simpl.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_cas_suc_r Δ Γ E K l e1 e2 v1 v2 v t τ :
    nclose specN ⊆ E →
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v = v1 →
    l ↦ₛ v -∗
    (l ↦ₛ v2 -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K #true : τ)
    -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K (CAS #l e1 e2) : τ.
  Proof.
    iIntros (????) "Hl Hlog".
    pose (Φ := (fun w => ⌜w = #true⌝ ∗ l ↦ₛ v2)%I).
    iApply (bin_log_related_step_r Φ with "[Hl]"); eauto.
    { cbv[Φ].
      iIntros (ρ j K') "#Hs Hj /=".
      tp_cas_suc j; auto.
      iExists #true. simpl.
      iFrame. eauto. }
    iIntros (w) "[% Hl]"; subst.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_fork_r Δ Γ E K (e t : expr) (τ : type)
   (Hmasked : nclose specN ⊆ E)
   (Hclosed : Closed ∅ e) :
   (∀ i, i ⤇ e -∗
     {E;Δ;Γ} ⊨ t ≤log≤ fill K #() : τ) -∗
   {E;Δ;Γ} ⊨ t ≤log≤ fill K (Fork e) : τ.
  Proof.
    iIntros "Hlog".
    pose (Φ := (fun (v : val) => ∃ i, i ⤇ e ∗ ⌜v = #()⌝%V)%I).
    iApply (bin_log_related_step_r Φ with "[]"); cbv[Φ].
    { iIntros (ρ j K') "#Hspec Hj".
      tp_fork j as i "Hi".
      iModIntro. iExists #(). iFrame. eauto.
    }
    iIntros (v). iDestruct 1 as (i) "[Hi %]"; subst.
    by iApply "Hlog".
  Qed.

  (** [bin_log_related_var] is in [fundamental_binary.v] *)
  (** [bin_log_related_rec] is in [fundamental_binary.v] *)
  (** [bin_log_related_tlam] is in [fundamental_binary.v] *)
  (** [bin_log_related_tapp] is in [fundamental_binary.v] *)
  (** [bin_log_related_pack] is in [fundamental_binary.v] *)
  (** [bin_log_related_unpack] is in [fundamental_binary.v] *)
  (** [bin_log_related_fork] is in [fundamental_binary.v] *)

  (* -------------------------------------------------- *)
  (* -------------------------------------------------- *)
  (** * Derived rules *)
  (** Derived compatibility rules are in [fundamental_binary.v] *)
  (* -------------------------------------------------- *)
  (* -------------------------------------------------- *)

  Lemma bin_log_related_arrow Δ Γ (f x f' x' : binder) (f1 f2 eb eb' : expr) (τ τ' : type) :
    f1 = (rec: f x := eb)%E →
    f2 = (rec: f' x' := eb')%E →
    Closed ∅ f1 →
    Closed ∅ f2 →
    □(∀ (v1 v2 : val), □ ({Δ;Γ} ⊨ v1 ≤log≤ v2 : τ) -∗
      {Δ;Γ} ⊨ f1 v1 ≤log≤ f2 v2 : τ') -∗
    {Δ;Γ} ⊨ f1 ≤log≤ f2 : TArrow τ τ'.
  Proof.
    iIntros (????) "#H".
    iApply bin_log_related_arrow_val; eauto.
    iAlways. iIntros (v1 v2) "#Hvv".
    iApply "H". iAlways.
    iApply (related_ret ⊤).
    by iApply interp_ret.
  Qed.

  (** ** (Pure) reductions on the left *)

  Lemma bin_log_related_rec_l Δ Γ K (f x : binder) e e' v t τ
   (Hclosed : Closed (x :b: f :b: ∅) e) :
   (to_val e' = Some v) →
   ▷ ({Δ;Γ} ⊨ (fill K (subst' f (Rec f x e) (subst' x e' e))) ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (App (Rec f x e) e')) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_fst_l Δ Γ K v1 v2 e τ :
   ▷ ({Δ;Γ} ⊨ fill K (of_val v1) ≤log≤ e : τ)
   ⊢ {Δ;Γ} ⊨ fill K (Fst (Pair (of_val v1) (of_val v2))) ≤log≤ e : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_snd_l Δ Γ K v1 v2 e τ :
   ▷ ({Δ;Γ} ⊨ (fill K (of_val v2)) ≤log≤ e : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (Snd (Pair (of_val v1) (of_val v2)))) ≤log≤ e : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_tlam_l Δ Γ K e t τ
   (Hclosed : Closed ∅ e) :
   ▷ ({Δ;Γ} ⊨ fill K e ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (TApp (TLam e))) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_fold_l Δ Γ K e v t τ :
   (to_val e = Some v) →
   ▷ ({Δ;Γ} ⊨ fill K e ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (Unfold (Fold e))) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_pack_l Δ Γ K e e' v t τ :
   to_val e = Some v →
   ▷ ({Δ;Γ} ⊨ fill K (App e' e) ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ fill K (Unpack (Pack e) e') ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_case_inl_l Δ Γ K e v e1 e2 t τ :
   to_val e = Some v →
   ▷ ({Δ;Γ} ⊨ fill K (App e1 e) ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ fill K (Case (InjL e) e1 e2) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_case_inr_l Δ Γ K e v e1 e2 t τ :
   to_val e = Some v →
   ▷ ({Δ;Γ} ⊨ fill K (App e2 e) ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ fill K (Case (InjR e) e1 e2) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_if_true_l Δ Γ K e1 e2 t τ :
   ▷ ({Δ;Γ} ⊨ fill K e1 ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ fill K (If #true e1 e2) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_if_false_l Δ Γ K e1 e2 t τ :
   ▷ ({Δ;Γ} ⊨ fill K e2 ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (If #false e1 e2)) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  Lemma bin_log_related_binop_l Δ Γ K op e1 e2 v1 v2 v t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    binop_eval op v1 v2 = Some v →
   ▷ ({Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ)
   ⊢ {Δ;Γ} ⊨ (fill K (BinOp op e1 e2)) ≤log≤ t : τ.
  Proof. iIntros. by iApply bin_log_pure_l. Qed.

  (** ** (Pure) reductions on the RHS. *)

  Lemma bin_log_related_rec_r Δ Γ E K f x e e' t v' τ
   (Hspec : nclose specN ⊆ E)
   (Hclosed : Closed (x :b: f :b: ∅) e) :
   (to_val e' = Some v') →
   {E;Δ;Γ} ⊨ t ≤log≤ fill K (subst' f (Rec f x e) (subst' x e' e)) : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ fill K (App (Rec f x e) e') : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_fst_r Δ Γ E K e v1 v2 τ
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ e ≤log≤ (fill K (of_val v1)) : τ
   ⊢ {E;Δ;Γ} ⊨ e ≤log≤ (fill K (Fst (Pair (of_val v1) (of_val v2)))) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_snd_r Δ Γ E K e v1 v2 τ
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ e ≤log≤ (fill K (of_val v2)) : τ
   ⊢ {E;Δ;Γ} ⊨ e ≤log≤ (fill K (Snd (Pair (of_val v1) (of_val v2)))) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_tlam_r Δ Γ E K e t τ
   (Hclosed : Closed ∅ e)
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ t ≤log≤ fill K e : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ fill K (TApp (TLam e)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_fold_r Δ Γ E K e v t τ
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ t ≤log≤ fill K e : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ fill K (Unfold (Fold e)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_pack_r Δ Γ E K e e' v t τ
   (Hclosed : Closed ∅ e')
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ t ≤log≤ fill K (App e' e) : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ fill K (Unpack (Pack e) e') : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_case_inl_r Δ Γ E K e v e1 e2 t τ
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2)
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ t ≤log≤ (fill K (App e1 e)) : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ (fill K (Case (InjL e) e1 e2)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_case_inr_r Δ Γ E K e v e1 e2 t τ
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2)
   (Hval : to_val e = Some v)
   (Hspec : nclose specN ⊆ E) :
   {E;Δ;Γ} ⊨ t ≤log≤ (fill K (App e2 e)) : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ (fill K (Case (InjR e) e1 e2)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_if_true_r Δ Γ E K e e1 e2 τ
   (Hspec : nclose specN ⊆ E)
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2) :
   {E;Δ;Γ} ⊨ e ≤log≤ (fill K e1) : τ
   ⊢ {E;Δ;Γ} ⊨ e ≤log≤ (fill K (If #true e1 e2)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_if_false_r Δ Γ E K e e1 e2 τ
   (Hspec : nclose specN ⊆ E)
   (Hclosed1 : Closed ∅ e1)
   (Hclosed2 : Closed ∅ e2) :
   {E;Δ;Γ} ⊨ e ≤log≤ (fill K e2) : τ
   ⊢ {E;Δ;Γ} ⊨ e ≤log≤ (fill K (If #false e1 e2)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  Lemma bin_log_related_binop_r Δ Γ E K op e1 e2 v1 v2 v t τ
   (Hspec : nclose specN ⊆ E) :
   to_val e1 = Some v1 →
   to_val e2 = Some v2 →
   binop_eval op v1 v2 = Some v →
   {E;Δ;Γ} ⊨ t ≤log≤ fill K (of_val v) : τ
   ⊢ {E;Δ;Γ} ⊨ t ≤log≤ (fill K (BinOp op e1 e2)) : τ.
  Proof. iIntros. iApply bin_log_pure_r; eauto. Qed.

  (** ** Stateful reductions on the LHS *)

  Lemma bin_log_related_fork_l Δ Γ E K (e t : expr) (τ : type)
   (Hclosed : Closed ∅ e) :
   (|={⊤,E}=> ▷ (WP e {{ _, True }}) ∗
    {E;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ) -∗
   {Δ;Γ} ⊨ fill K (Fork e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as "[Hsafe Hlog]". iModIntro.
    iApply wp_fork. iNext. by iFrame "Hsafe".
  Qed.

  Lemma bin_log_related_alloc_l Δ Γ E K e v t τ
    (Heval : to_val e = Some v) :
    (|={⊤,E}=> ▷ (∀ l : loc, l ↦ᵢ v -∗
           {E;Δ;Γ} ⊨ fill K (# l) ≤log≤ t : τ))%I
    -∗ {Δ;Γ} ⊨ fill K (ref e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog". iModIntro.
    iApply (wp_alloc _ _ v); auto.
  Qed.

  Lemma bin_log_related_alloc_l' Δ Γ K e v t τ
    (Heval : to_val e = Some v) :
    ▷ (∀ (l : loc), l ↦ᵢ v -∗ {Δ;Γ} ⊨ fill K (# l) ≤log≤ t : τ)%I
    -∗ {Δ;Γ} ⊨ fill K (ref e) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply (bin_log_related_alloc_l); auto. assumption.
  Qed.

  Lemma bin_log_related_load_l Δ Γ E K l q t τ :
    (|={⊤,E}=> ∃ v',
      ▷(l ↦ᵢ{q} v') ∗
      ▷(l ↦ᵢ{q} v' -∗ ({ E;Δ;Γ } ⊨ fill K (of_val v') ≤log≤ t : τ)))%I
    -∗ {Δ;Γ} ⊨ fill K (! #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl Hlog]". iModIntro.
    iApply (wp_load with "Hl"); auto.
  Qed.

  Lemma bin_log_related_load_l' Δ Γ K l q v t τ :
    ▷ l ↦ᵢ{q} v -∗
    ▷ (l ↦ᵢ{q} v -∗ ({Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ))
    -∗ {Δ;Γ} ⊨ fill K !#l ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    iApply (bin_log_related_load_l); auto.
    iExists v.
    iModIntro.
    by iFrame.
  Qed.

  Lemma bin_log_related_store_l Δ Γ E K l e v' t τ :
    to_val e = Some v' →
    (|={⊤,E}=> ∃ v, ▷ l ↦ᵢ v ∗
      ▷(l ↦ᵢ v' -∗ {E;Δ;Γ} ⊨ fill K #() ≤log≤ t : τ))
    -∗ {Δ;Γ} ⊨ fill K (#l <- e) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v) "[Hl Hlog]". iModIntro.
    iApply (wp_store _ _ _ _ v' with "Hl"); auto.
  Qed.

  Lemma bin_log_related_store_l' Δ Γ K l e v v' t τ :
    to_val e = Some v' →
    ▷ (l ↦ᵢ v) -∗
    ▷ (l ↦ᵢ v' -∗ {Δ;Γ} ⊨ fill K #() ≤log≤ t : τ)
    -∗ {Δ;Γ} ⊨ fill K (#l <- e) ≤log≤ t : τ.
  Proof.
    iIntros (?) "Hl Hlog".
    iApply bin_log_related_store_l; eauto.
    iModIntro. iExists v. iFrame.
  Qed.

  Lemma bin_log_related_cas_l Δ Γ E K l e1 e2 v1 v2 t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    (|={⊤,E}=> ∃ v', ▷ l ↦ᵢ v' ∗
     (⌜v' ≠ v1⌝ -∗ ▷ (l ↦ᵢ v' -∗ {E;Δ;Γ} ⊨ fill K #false ≤log≤ t : τ)) ∧
     (⌜v' = v1⌝ -∗ ▷ (l ↦ᵢ v2 -∗ {E;Δ;Γ} ⊨ fill K #true ≤log≤ t : τ)))
    -∗ {Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl Hlog]". iModIntro.
    destruct (decide (v' = v1)).
    - (* CAS successful *) subst.
      iApply (wp_cas_suc with "Hl"); eauto.
      iDestruct "Hlog" as "[_ Hlog]".
      iSpecialize ("Hlog" with "[]"); eauto.
    - (* CAS failed *)
      iApply (wp_cas_fail with "Hl"); eauto.
      iDestruct "Hlog" as "[Hlog _]".
      iSpecialize ("Hlog" with "[]"); eauto.
  Qed.

  Lemma bin_log_related_cas_fail_l Δ Γ E K l e1 e2 v1 v2 t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    (|={⊤,E}=> ∃ v', ▷ l ↦ᵢ v' ∗ ⌜v' ≠ v1⌝ ∗
     (l ↦ᵢ v' -∗ {E;Δ;Γ} ⊨ fill K #false ≤log≤ t : τ))
    -∗ {Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as (v') "[Hl [% Hlog]]". iModIntro.
    iApply (wp_cas_fail with "Hl"); eauto.
  Qed.

  Lemma bin_log_related_cas_fail_l' Δ Γ K l e1 e2 v1 v2 v' t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v' ≠ v1 →
    ▷ (l ↦ᵢ v') -∗
    (l ↦ᵢ v' -∗ {Δ;Γ} ⊨ fill K #false ≤log≤ t : τ)
    -∗ {Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (???) "Hl Hlog".
    iApply bin_log_related_cas_fail_l; eauto.
    iModIntro. iExists v'. iFrame "Hl Hlog". eauto.
  Qed.

  Lemma bin_log_related_cas_suc_l Δ Γ E K l e1 e2 v1 v2 t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    (|={⊤,E}=> ▷ (l ↦ᵢ v1) ∗
     (l ↦ᵢ v2 -∗ {E;Δ;Γ} ⊨ fill K #true ≤log≤ t : τ))
    -∗ {Δ;Γ} ⊨ fill K (CAS #l e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (??) "Hlog".
    iApply bin_log_related_wp_atomic_l; auto.
    iMod "Hlog" as "[Hl Hlog]". iModIntro.
    iApply (wp_cas_suc with "Hl"); eauto.
  Qed.

  Lemma bin_log_related_cas_suc_l' Δ Γ K l e1 e2 v1 v2 v' t τ :
    to_val e1 = Some v1 →
    to_val e2 = Some v2 →
    v' = v1 →
    ▷ l ↦ᵢ v' -∗
    (l ↦ᵢ v2 -∗ {Δ;Γ} ⊨ fill K #true ≤log≤ t : τ)
    -∗ {Δ;Γ} ⊨ fill K (CAS (# l) e1 e2) ≤log≤ t : τ.
  Proof.
    iIntros (???) "Hl Hlog". subst.
    iApply bin_log_related_cas_suc_l; eauto.
    iModIntro. iFrame.
  Qed.

End properties.

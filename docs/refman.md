writing programs
================

Typechecking your programs automatically
----------------------------------------

`iris-logrel` supports (semi)-automatic typechecking of programs in F_mu_ref_conc

1. Make sure that you import modules `F_mu_ref_conc.typing` and `F_mu_ref_conc.reflection`
   which contain tactics for solving typeability and closendess.
2. Define your program, usually as a closed value.
```coq
Definition prog : val := λ: "x", if: !"x" then (λ: <>, 1) else (λ: <>, 0).
```
3. Typecheck it, in an arbitrary context, using the `solve_typed` tactic.
```coq
Lemma prog_typed Γ : Γ ⊢ₜ prog : TArrow (Tref TBool) (TArrow TUnit TNat).
Proof.
  solve_typed.
Qed.
```
4. Add the typeability lemma to the common database.
```coq
Hint Resolve prog_typed : typeable.
```

It is important that in step 3 your lemma is general enough. Once you
add the lemma to the hint database (step 4), it can be used in
typechecking compound expressions containing values already
typechecked before, for example:

```coq
Definition prog2 : val := λ: <>,
  let: "x" := ref false in 
  prog "x" ().

Lemma prog2_typed Γ : Γ ⊢ₜ prog2 : TArrow TUnit TNat.
Proof. solve_typed. Qed.

Hint Resolve prog2_typed : typeable.
```

tactics/proofmode reference manual
==================================

Threadpool tactics
------------------

All of the tactics in this section assume that you have a resource `j ⤇ e` 
and a goal of the form `|={E}=>` with `↑specN ⊆ E`, e.g. `P ∗ (j ⤇ e) ⊢ |={⊤}=> Q`.

The tactics below allow for a symbolic execution of the logical
threadpool.

- `tp_normalise j`: simplify the term/expression in `j ⤇ e`
- `tp_bind j e'`: if `j ⤇ e` and `e` can be decomposed as `K[e']`,
  then turn the resource into `j ⤇ K[e']`. For example if, `j ⤇ f (!x + 2)`,
  then invoking `tp_bind j (Load x)` converts the resource
  to `j ⤇ fill [AppRCtx f, PlusLCtx 2] (!x)`. All the tactics below,
  apart from `tp_apply` use this bind facility to find a subterm in
  the threadpool of an appropriate shape.
- `tp_store j`: if `j ⤇ K[#l <- v2]` and `l ↦ₛ v1` are in the
  context, then replace them with `j ⤇ K[#()]` and `l ↦ₛ v2`
- `tp_load j`: if `j ⤇ K[! #l]` and `l ↦ₛ v` are in the context,
  then replace them with `j ⤇ K[v]` and `l ↦ₛ v`.
- `tp_rec j`: if `j ⤇ K[(rec f x := e) v]` (or `j ⤇ K[(λx. e) v]`)
   is in the context, then replace it with `j ⤇ K[e[x:=v,f:=(rec f x := e)]]`
   (or `j ⤇ K[e[x:=v]]`).
- `tp_op j`/`tp_nat_binop j`: if `j ⤇ K[v1 ⊕ v2]` is in the context, then
  replace it with `j ⤇ K[v3]` whenever `v1 ⊕ v2` evaluates to `v3`.
- `tp_cas_fail j`: if `j ⤇ K[CAS #l v1 v2]` and `l ↦ₛ v'` are in the
  context and `v' ≠ v1` holds, then replace the threadpool resource with `j ⤇ K[#♭ false]`.
- `tp_cas_suc j`: if `j ⤇ K[CAS #l v1 v2]` and `l ↦ₛ v'` are in the
  context and `v' = v1` holds, then replace the threadpool resource with `j ⤇ K[#♭ true]`
  and replace `l ↦ₛ v'` with `l ↦ₛ v2`
- `tp_tlam j`: if `j ⤇ K[Λ.e]` is in the context, then replace it with
  `j ⤇ K[e]`
- `tp_fold j`: if `j ⤇ K[Unfold (Fold v)]` is in the context, then replace it with
  `j ⤇ K[v]`
- `tp_pack j`: if `j ⤇ K[unpack v e]` is in the context, then replace
  it with `j ⤇ K[e v]`
- `tp_fst j`, `tp_snd j`: if `j ⤇ K[fst (v1,v2)]` (resp. `j ⤇ K[snd (v1,v2)]`)
  is in the context, then replace it with `j ⤇ K[v1]` (resp. `j ⤇ K[v2]`)
- `tp_case_inl j`,`tp_case_inr j`: if `j ⤇ K[case (inl v) e1 e2]` (resp. `j ⤇ K[case (inr v) e1 e2]`)
  is in the context, the replace it with `j ⤇ K[e1 v]` (resp. `j ⤇ K[e2 v]`)
- `tp_if_true j`,`tp_if_false j`: if `j ⤇ K[if (#♭ true) e1 e2]` (resp. `j ⤇ K[if (#♭ false) e1 e2]`)
  is in the context, the replace it with `j ⤇ K[e1]` (resp. `j ⤇ K[e2]`)
- `tp_fork j as i "Hi"`: if `j ⤇ K[fork(e)]` is in the context, then replace it with `j ⤇ K[#()]`
  and add `Hi: i ⤇ e` to the context.
  Additional forms:
    + `tp_fork i as j` is the same thing as `let H := iFresh in tp_fork i as j H`
    + `tp_fork i` has a similar behaviour, but changes your goal from `|={E}=> Q` to
    `∀ (j' : nat), j' ⤇ e' ={E}=∗ Q`.
- `tp_alloc j as l "Hl"`: if `j ⤇ K[alloc v]` is in the context, then replace it with `j ⤇ K[#l]`
  and add `Hl : l ↦ₛ v` to the context.
  Additional forms:
    + `tp_alloc i as j` is the same thing as `let H := iFresh in tp_alloc i as j H`
- `tp_apply j lem with "H1 ... Hn" as "S"`: **TODO**

Weakest precondition calculus tactics
-------------------------------------

- `wp_bind e'`: if the goal is `WP e @ E {{ Ψ }}` and `e` can be
  decomposed as `K[e']`, then turn the goal into `WP e' @ E {{ v, WP K[v] @ E {{ Q }} }}`

Tactics for internal use
------------------------

- `reshape_expr e tac`: tries to non-deterministically decompose `e` as `K[e']` and calls `tac K e'`

TODO: `inv_head_step`, `rewrite_closed`, `solve_closed`, `rel_bind_l/r`, `reflection.of_expr`, `value_case`
